package controllers;

import com.google.gson.Gson;
import dto.HallDTO;
import dto.MovieSessionDTO;
import dto.PlaceDTO;
import dto.TicketDTO;
import model.MovieSession;
import service.impl.HallServiceImpl;
import service.impl.MovieSessionServiceImpl;
import service.impl.PlaceServiceImpl;
import service.impl.TicketServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


@WebServlet(name = "ShowChoosePlaceServlet", urlPatterns = {"/showChoosePlace"})
public class ShowChoosePlaceServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        if(request.getParameter("method").contains("sh")) {
            HallDTO hallDTO = HallServiceImpl.getInstance().getById(Integer.parseInt(request.getParameter("id")));
            request.setAttribute("hallDTO", hallDTO);
            List<PlaceDTO> placeDTOList = PlaceServiceImpl.getInstance().getBy_All(Integer.parseInt(request.getParameter("id")));

            List<Integer> row = new ArrayList<>();
            for (PlaceDTO placeDTO : placeDTOList) {
                row.add(placeDTO.getPlaceNumber());
            }

            request.setAttribute("row", row);
            request.setAttribute("placeDTOList", placeDTOList);
            request.getRequestDispatcher("pages/common/show_place.jsp").forward(request, response);

        } else if(request.getParameter("method").contains("ch")){
            MovieSessionDTO movieSessionDTO = MovieSessionServiceImpl.getInstance().getById(Integer.parseInt(request.getParameter("session_id")));
            request.setAttribute("movieSessionDTO", movieSessionDTO);
            request.setAttribute("movieSessionID", request.getParameter("session_id"));
            Integer hallId = movieSessionDTO.getHall().getId();
            String hallName = movieSessionDTO.getHall().getName();
            request.setAttribute("hallId", hallId);
            request.setAttribute("hallName", hallName);
            List<PlaceDTO> placeDTOList = PlaceServiceImpl.getInstance().getBy_All(hallId);
            /*List<Integer> row = new ArrayList<>();
            for (PlaceDTO placeDTO : placeDTOList) {
                row.add(placeDTO.getPlaceNumber());
            }*/

            List<TicketDTO> ticketDTOList = TicketServiceImpl.getInstance().getAll_ByMovieSession(Integer.parseInt(request.getParameter("session_id")));
            request.setAttribute("ticketDTOList", ticketDTOList);


           // request.setAttribute("row", row);
            request.setAttribute("placeDTOList", placeDTOList);
            request.getRequestDispatcher("pages/common/choose_place.jsp").forward(request, response);
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request,response);
    }
}
