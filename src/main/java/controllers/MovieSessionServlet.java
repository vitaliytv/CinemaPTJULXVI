package controllers;

import dto.HallDTO;
import dto.MovieDTO;
import dto.MovieSessionDTO;
import dto.UserDTO;
import service.impl.HallServiceImpl;
import service.impl.MovieServiceImpl;
import service.impl.MovieSessionServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.*;


@WebServlet(name = "MovieSessionServlet", urlPatterns = {"/movieSession"})
public class MovieSessionServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        UserDTO userDTO = (UserDTO) request.getSession().getAttribute("user");
        if (userDTO != null && userDTO.getRole().getName().equalsIgnoreCase("user")) {
            request.setAttribute("user", userDTO);
        }
        List<MovieDTO> movieDTOList = MovieServiceImpl.getInstance().getAll();
        List<HallDTO> hallDTOList = HallServiceImpl.getInstance().getAll();

        request.setAttribute("movieDTOList", movieDTOList);
        request.setAttribute("hallDTOList", hallDTOList);
        LocalDateTime currentDateTime = LocalDateTime.now();
        List<MovieSessionDTO> movieSessionDTOList = MovieSessionServiceImpl.getInstance().getByCurrentDate(currentDateTime);



        Set<LocalDateTime> localDateHashSet = new TreeSet<>();
        DateTimeFormatter FORMAT = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        for (MovieSessionDTO movieSessionDTO : movieSessionDTOList) {
            if(!localDateHashSet.contains(movieSessionDTO.getDate())){
                localDateHashSet.add(movieSessionDTO.getDate());
            }
        }
        request.setAttribute("FORMAT", FORMAT);
        request.setAttribute("localDateHashSet", localDateHashSet);
        movieSessionDTOList.sort(new Comparator<MovieSessionDTO>() {
            @Override
            public int compare(MovieSessionDTO movieSessionDTO, MovieSessionDTO t1) {
                return  movieSessionDTO.getMovie().getName().compareTo(t1.getMovie().getName()) & movieSessionDTO.getDate().compareTo(t1.getDate()) &
                        movieSessionDTO.getHall().getName().compareTo(t1.getHall().getName()) & movieSessionDTO.getName().compareTo(t1.getName());
            }
        });
        request.setAttribute("movieSessionDTOList", movieSessionDTOList);

        request.setAttribute("currentTime", currentDateTime);

        request.getRequestDispatcher("/pages/common/movie_session.jsp").forward(request,response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request,response);
    }
}
