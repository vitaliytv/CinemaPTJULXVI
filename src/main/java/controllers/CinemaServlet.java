package controllers;

import dto.HallDTO;
import dto.UserDTO;
import service.impl.HallServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;


@WebServlet(name = "CinemaServlet", urlPatterns={"/cinemaDescription"})
public class CinemaServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        UserDTO userDTO = (UserDTO) request.getSession().getAttribute("user");
        if (userDTO != null && userDTO.getRole().getName().equalsIgnoreCase("user")) {
            request.setAttribute("user", userDTO);
        }
        List<HallDTO> hallDTOList = HallServiceImpl.getInstance().getAll();
        request.setAttribute("hallDTOList", hallDTOList);
        request.getRequestDispatcher("pages/common/cinema.jsp").forward(request,response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request,response);
    }
}
