package controllers;

import dto.RoleDTO;
import dto.UserDTO;
import model.User;
import service.impl.RoleServiceImpl;
import service.impl.UserServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Date;


@WebServlet(name = "RegistrationServlet", urlPatterns = {"/registration"})
public class RegistrationServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("UTF-8");

        String name = request.getParameter("name");
        String surname = request.getParameter("surname");
        String email = request.getParameter("email");
        String login = request.getParameter("login");
        String password = request.getParameter("password");
        String repassword = request.getParameter("repassword");
        Integer phone = Integer.parseInt(request.getParameter("phone"));
        DateTimeFormatter FORMAT = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate birthday = LocalDate.parse(request.getParameter("birthday"),FORMAT);//(new SimpleDateFormat("dd-MM-yyyy").parse(request.getParameter("birthday")));
        String sex = request.getParameter("sex");

        UserServiceImpl.getInstance().save(new UserDTO(name, surname, email, login, password, phone, birthday, sex, RoleServiceImpl.getInstance().getById(2)));
        request.getRequestDispatcher("pages/common/login.jsp").forward(request,response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request,response);
    }
}
