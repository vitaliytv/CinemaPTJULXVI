package controllers.admin.list;

import dto.HallDTO;
import dto.MovieDTO;
import dto.MovieSessionDTO;
import service.impl.HallServiceImpl;
import service.impl.MovieServiceImpl;
import service.impl.MovieSessionServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;


@WebServlet(name = "MovieSessionListServlet", urlPatterns = {"/admin/list/SessionList"})
public class MovieSessionListServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        List<MovieDTO> movieDTOList = MovieServiceImpl.getInstance().getAll();
        List<HallDTO> hallDTOList = HallServiceImpl.getInstance().getAll();

        request.setAttribute("movieDTOList", movieDTOList);
        request.setAttribute("hallDTOList", hallDTOList);
        List<MovieSessionDTO> movieSessionDTOList = MovieSessionServiceImpl.getInstance().getAll();
        Set<LocalDateTime> localDateHashSet = new TreeSet<>();
        for (MovieSessionDTO movieSessionDTO : movieSessionDTOList) {
            if(!localDateHashSet.contains(movieSessionDTO.getDate())){
                localDateHashSet.add(movieSessionDTO.getDate());
            }
        }
        request.setAttribute("localDateHashSet", localDateHashSet);
        DateTimeFormatter FORMAT = DateTimeFormatter.ofPattern("dd-MM-yyyy");
        request.setAttribute("FORMAT", FORMAT);
        movieSessionDTOList.sort(new Comparator<MovieSessionDTO>() {
            @Override
            public int compare(MovieSessionDTO movieSessionDTO, MovieSessionDTO t1) {
                return  movieSessionDTO.getMovie().getName().compareTo(t1.getMovie().getName()) & movieSessionDTO.getDate().compareTo(t1.getDate()) &
                        movieSessionDTO.getHall().getName().compareTo(t1.getHall().getName()) & movieSessionDTO.getName().compareTo(t1.getName());
            }
        });
        request.setAttribute("movieSessionDTOList", movieSessionDTOList);
        //response.sendRedirect(request.getContextPath() + "/movie_list.jsp");
        request.getRequestDispatcher("/pages/admin/moviesession_list.jsp").forward(request,response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request,response);
    }

    private void calculate(){


    }
}
