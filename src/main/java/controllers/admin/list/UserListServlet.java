package controllers.admin.list;

import dto.RoleDTO;
import dto.UserDTO;
import service.impl.RoleServiceImpl;
import service.impl.UserServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * Created by Vitaliy on 17.10.2016.
 */
@WebServlet(name = "UserListServlet", urlPatterns = {"/admin/list/UserList"})
public class UserListServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        List<UserDTO> userDTOList = UserServiceImpl.getInstance().getAll();
        List<RoleDTO> roleDTOList = RoleServiceImpl.getInstance().getAll();
        request.setAttribute("userDTOList", userDTOList);
        request.setAttribute("roleDTOList", roleDTOList);
        //response.sendRedirect(request.getContextPath() + "/movie_list.jsp");
        request.getRequestDispatcher("/pages/admin/user_list.jsp").forward(request,response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request,response);
    }
}
