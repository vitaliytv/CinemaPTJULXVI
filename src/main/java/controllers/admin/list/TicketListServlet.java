package controllers.admin.list;

import dto.TicketDTO;
import model.Ticket;
import service.impl.TicketServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.time.format.DateTimeFormatter;
import java.util.Comparator;
import java.util.List;

/**
 * Created by Vitaliy on 03.11.2016.
 */
@WebServlet(name = "TicketListServlet", urlPatterns = {"/admin/list/TicketList"})
public class TicketListServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        List<TicketDTO> ticketDTOList = TicketServiceImpl.getInstance().getAll();

        ticketDTOList.sort(new Comparator<TicketDTO>() {
            @Override
            public int compare(TicketDTO ticketDTO, TicketDTO t1) {
                return ticketDTO.getDate().compareTo(t1.getDate());
            }
        });

        request.setAttribute("ticketDTOList", ticketDTOList);

        DateTimeFormatter FORMAT = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        request.setAttribute("FORMAT", FORMAT);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        request.setAttribute("sdf", sdf);

        request.getRequestDispatcher("/pages/admin/ticket_list.jsp").forward(request,response);

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request,response);
    }
}
