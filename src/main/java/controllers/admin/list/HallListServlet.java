package controllers.admin.list;

import dto.HallDTO;
import dto.PlaceDTO;
import service.impl.HallServiceImpl;
import service.impl.PlaceServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;


@WebServlet(name = "HallListServlet", urlPatterns = {"/admin/list/HallList"})
public class HallListServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        List<HallDTO> hallDTOList = HallServiceImpl.getInstance().getAll();
        List<PlaceDTO> placeDTOList = PlaceServiceImpl.getInstance().getAll();
        request.setAttribute("hallDTOList", hallDTOList);
        request.setAttribute("placeDTOList", placeDTOList);

        request.getRequestDispatcher("/pages/admin/hall_list.jsp").forward(request,response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request,response);
    }
}
