package controllers.admin.update;

import dto.HallDTO;
import dto.PlaceDTO;
import service.impl.HallServiceImpl;
import service.impl.PlaceServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Vitaliy on 29.10.2016.
 */
@WebServlet(name = "HallUpdateServlet", urlPatterns = {"/admin/updateHall"})
public class HallUpdateServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("UTF-8");
        if(request.getParameter("method").contains("sh")){
            Integer id = Integer.parseInt(request.getParameter("id"));
            HallDTO hallDTO = HallServiceImpl.getInstance().getById(id);
            request.setAttribute("hallDTO", hallDTO);
            List<PlaceDTO> placeDTOList = PlaceServiceImpl.getInstance().getBy_All(id);
            request.setAttribute("placeDTOList", placeDTOList);

            request.getRequestDispatcher("/pages/admin/update/hall_update.jsp").forward(request,response);
        }
        else if(request.getParameter("method").contains("sv")){
            Integer id = Integer.parseInt(request.getParameter("id"));
            String name = request.getParameter("name");
            String description = request.getParameter("description");
            HallDTO hallDTO = new HallDTO(name,description);
            hallDTO.setId(id);
            HallServiceImpl.getInstance().update(hallDTO);

            Enumeration<String> paramNames = request.getParameterNames();
            Pattern pattern = Pattern.compile("row_(\\d+)");
            Integer row_number, place_number;
            Map<Integer, Integer> integerMap = new HashMap<>();
            while(paramNames.hasMoreElements()){
                String paramName = paramNames.nextElement();
                Matcher m = pattern.matcher(paramName);
                if (m.find()) {
                    row_number = Integer.parseInt(m.group(1));
                    place_number = Integer.parseInt(request.getParameter(paramName));
                    integerMap.put(row_number,place_number);
                    /*for (PlaceDTO placeDTO : placeDTOList) {
                        int iterator = 0;
                        if(placeDTO.getRowNumber() == row_number){
                            PlaceServiceImpl.getInstance().update(new PlaceDTO(row_number, place_number, hallDTO))
                        }*/
                    }

                }

            //PlaceServiceImpl.getInstance().save(new PlaceDTO(row_number, place_number, hallDTO));
            List<PlaceDTO> placeDTOList = PlaceServiceImpl.getInstance().getBy_All(id);
            for (PlaceDTO placeDTO : placeDTOList) {
                if(!integerMap.containsKey(placeDTO.getRowNumber())){
                    PlaceServiceImpl.getInstance().delete(placeDTO.getId());
                }
            }
            placeDTOList = PlaceServiceImpl.getInstance().getBy_All(id);

            for (Map.Entry<Integer, Integer> entry : integerMap.entrySet()) {
                int options = 0;
                for (PlaceDTO placeDTO : placeDTOList) {
                    if(entry.getKey() == placeDTO.getRowNumber()){
                        PlaceDTO newPlaceDTO = new PlaceDTO(placeDTO.getRowNumber(), entry.getValue(), hallDTO);
                        newPlaceDTO.setId(placeDTO.getId());
                        PlaceServiceImpl.getInstance().update(newPlaceDTO);
                        options = 1;
                        break;
                    }
                }
                if(options ==0){
                    PlaceServiceImpl.getInstance().save(new PlaceDTO(entry.getKey(),entry.getValue(),hallDTO));
                }

            }

            request.getRequestDispatcher("/admin/list/HallList").forward(request,response);

        } else if(request.getParameter("method").contains("dh")){
            Integer id = Integer.parseInt(request.getParameter("id"));
            HallServiceImpl.getInstance().delete(id);

            request.getRequestDispatcher("/admin/list/HallList").forward(request,response);
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request,response);
    }
}
