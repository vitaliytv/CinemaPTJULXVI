package controllers.admin.update;

import dto.HallDTO;
import dto.MovieDTO;
import dto.MovieSessionDTO;
import service.impl.HallServiceImpl;
import service.impl.MovieServiceImpl;
import service.impl.MovieSessionServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

/**
 * Created by Vitaliy on 29.10.2016.
 */
@WebServlet(name = "MovieSessionUpdateServlet", urlPatterns = {"/admin/updateMovieSession"})
public class MovieSessionUpdateServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("UTF-8");
        if(request.getParameter("method").contains("sms")){
            MovieSessionDTO movieSessionDTO = MovieSessionServiceImpl.getInstance().getById(Integer.parseInt(request.getParameter("id")));
            request.setAttribute("movieSessionDTO", movieSessionDTO);

            List<MovieDTO> movieDTOList = MovieServiceImpl.getInstance().getAll();
            List<HallDTO> hallDTOList = HallServiceImpl.getInstance().getAll();
            request.setAttribute("movieDTOList", movieDTOList);
            request.setAttribute("hallDTOList", hallDTOList);

            request.getRequestDispatcher("/pages/admin/update/moviesession_update.jsp").forward(request,response);
        }
        else if(request.getParameter("method").contains("sv")){
            Integer id = Integer.parseInt(request.getParameter("id"));
            String name = request.getParameter("name");
            DateTimeFormatter FORMAT = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm");
            LocalDateTime date = LocalDateTime.parse(request.getParameter("date"), FORMAT);
            Integer movie = Integer.parseInt(request.getParameter("movie[]"));
            Integer hall = Integer.parseInt(request.getParameter("hall[]"));

            MovieSessionDTO movieSessionDTO = new MovieSessionDTO(name, date, MovieServiceImpl.getInstance().getById(movie), HallServiceImpl.getInstance().getById(hall));
            movieSessionDTO.setId(id);
            MovieSessionServiceImpl.getInstance().update(movieSessionDTO);

            request.getRequestDispatcher("/admin/list/SessionList").forward(request,response);

        } else if(request.getParameter("method").contains("dms")){
            Integer id = Integer.parseInt(request.getParameter("id"));
            MovieSessionServiceImpl.getInstance().delete(id);

            request.getRequestDispatcher("/admin/list/SessionList").forward(request,response);
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request,response);
    }
}
