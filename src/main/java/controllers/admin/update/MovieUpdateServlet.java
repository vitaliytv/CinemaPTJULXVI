package controllers.admin.update;

import dto.MovieDTO;
import service.impl.MovieServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Enumeration;

/**
 * Created by Vitaliy on 29.10.2016.
 */
@WebServlet(name = "MovieUpdateServlet", urlPatterns = {"/admin/updateMovie"})
public class MovieUpdateServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("UTF-8");
        if(request.getParameter("method").contains("sm")){
            MovieDTO movieDTO = MovieServiceImpl.getInstance().getById(Integer.parseInt(request.getParameter("id")));
            request.setAttribute("movieDTO", movieDTO);
            request.getRequestDispatcher("/pages/admin/update/movie_update.jsp").forward(request,response);
        }
        else if(request.getParameter("method").contains("sv")){
            Integer id = Integer.parseInt(request.getParameter("id"));
            String name = request.getParameter("name");
            Integer year = Integer.parseInt(request.getParameter("year"));
            String country = request.getParameter("country");
            String genre = request.getParameter("genre");
            String starring = request.getParameter("starring");
            String director = request.getParameter("director");
            Integer length = Integer.parseInt(request.getParameter("length"));
            DateTimeFormatter FORMAT = DateTimeFormatter.ofPattern("yyyy-MM-dd");
            LocalDate rentStart = LocalDate.parse(request.getParameter("rent_start"),FORMAT);
            LocalDate rentEnd = LocalDate.parse(request.getParameter("rent_end"),FORMAT);
            String description = request.getParameter("description");
            String ageLimit = request.getParameter("age_limit");
            String image = request.getParameter("image");
            String smallImage = request.getParameter("small_image");
            MovieDTO movieDTO = new MovieDTO(name, year, country, genre, starring,
                           director, length, rentStart, rentEnd, description, ageLimit, image, smallImage);
            movieDTO.setId(id);
            MovieServiceImpl.getInstance().update(movieDTO);

            request.getRequestDispatcher("/admin/list/MovieList").forward(request,response);

        } else if(request.getParameter("method").contains("dm")){
            Integer id = Integer.parseInt(request.getParameter("id"));
            MovieServiceImpl.getInstance().delete(id);

            request.getRequestDispatcher("/admin/list/MovieList").forward(request,response);
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request,response);
    }
}
