package controllers.admin.create;

import dto.HallDTO;
import dto.MovieDTO;
import service.impl.HallServiceImpl;
import service.impl.MovieServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.List;

/**
 * Created by Vitaliy on 17.10.2016.
 */
@WebServlet(name = "MovieSessionCreateServlet", urlPatterns = {"/admin/createSession"})
public class MovieSessionCreateServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("UTF-8");
        List<MovieDTO> movieDTOList = MovieServiceImpl.getInstance().getAll();
        List<HallDTO> hallDTOList = HallServiceImpl.getInstance().getAll();
        request.setAttribute("movieDTOList", movieDTOList);
        request.setAttribute("hallDTOList", hallDTOList);

        //response.sendRedirect(request.getContextPath() + "/movie_list.jsp");
        request.getRequestDispatcher("/pages/admin/create/moviesession_create.jsp").forward(request,response);

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request,response);
    }
}
