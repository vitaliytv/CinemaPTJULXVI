package controllers.admin.create;

import dto.MovieSessionDTO;
import service.impl.HallServiceImpl;
import service.impl.MovieServiceImpl;
import service.impl.MovieSessionServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * Created by Vitaliy on 17.10.2016.
 */
@WebServlet(name = "MovieSessionSaveServlet", urlPatterns = {"/admin/saveSession"})
public class MovieSessionSaveServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("UTF-8");
        String name = request.getParameter("name");
        DateTimeFormatter FORMAT = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm");
        LocalDateTime date = LocalDateTime.parse(request.getParameter("date"), FORMAT);
        Integer movie = Integer.parseInt(request.getParameter("movie[]"));
        Integer hall = Integer.parseInt(request.getParameter("hall[]"));

        MovieSessionServiceImpl.getInstance().save(new MovieSessionDTO(name, date, MovieServiceImpl.getInstance().getById(movie), HallServiceImpl.getInstance().getById(hall)));
        response.sendRedirect(request.getContextPath() + "/pages/admin/admin.jsp");
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request,response);
    }
}
