package controllers.admin.create;

import dto.MovieDTO;
import service.impl.MovieServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;

/**
 * Created by Vitaliy on 09.10.2016.
 */
@WebServlet(name = "MovieCreateServlet", urlPatterns = {"/admin/createMovie"})
public class MovieCreateServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("UTF-8");
        String name = request.getParameter("name");
        Integer year = Integer.parseInt(request.getParameter("year"));
        String country = request.getParameter("country");
        String genre = request.getParameter("genre");
        String starring = request.getParameter("starring");
        String director = request.getParameter("director");
        Integer length = Integer.parseInt(request.getParameter("length"));
        DateTimeFormatter FORMAT = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate rentStart = LocalDate.parse(request.getParameter("rent_start"),FORMAT);
        LocalDate rentEnd = LocalDate.parse(request.getParameter("rent_end"),FORMAT);
        String description = request.getParameter("description");
        String ageLimit = request.getParameter("age_limit");
        String image = request.getParameter("image");
        String smallImage = request.getParameter("small_image");


        MovieServiceImpl.getInstance().save(new MovieDTO(name, year, country, genre, starring,
                director, length, rentStart, rentEnd, description, ageLimit, image, smallImage));
        response.sendRedirect(request.getContextPath() + "/pages/admin/admin.jsp");

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request,response);
    }
}
