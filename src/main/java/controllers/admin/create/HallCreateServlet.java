package controllers.admin.create;

import dto.HallDTO;
import dto.PlaceDTO;
import dto.UserDTO;
import service.impl.HallServiceImpl;
import service.impl.PlaceServiceImpl;
import service.impl.RoleServiceImpl;
import service.impl.UserServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Enumeration;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


@WebServlet(name = "HallCreateServlet", urlPatterns = {"/admin/createHall"})
public class HallCreateServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("UTF-8");

        String name = request.getParameter("name");
        String description = request.getParameter("description");
        HallServiceImpl.getInstance().save(new HallDTO(name,description));
        HallDTO hallDTO = HallServiceImpl.getInstance().getByName(name);

        /*Integer row_number = Integer.parseInt(request.getParameter("row_number"));
        Integer place_number = Integer.parseInt(request.getParameter("place_number"));

        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        String docType =
                "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0 " +
                        "Transitional//EN\">\n";
        String title = "Reading All Request Parameters";
        out.println(docType +
                "<HTML>\n" +
                "<HEAD><TITLE>"+title + "</TITLE></HEAD>\n"+
                "<BODY BGCOLOR=\"#FDF5E6\">\n" +
                "<H1 ALIGN=CENTER>" + title + "</H1>\n" +
                "<TABLE BORDER=1 ALIGN=CENTER>\n" +
                "<TR BGCOLOR=\"#FFAD00\">\n" +
                "<TH>Parameter Name<TH>Parameter Value(s)");*/
        Enumeration<String> paramNames = request.getParameterNames();
        Pattern pattern = Pattern.compile("row_(\\d+)");
        Integer row_number, place_number;
        while(paramNames.hasMoreElements()){
            String paramName = paramNames.nextElement();
            Matcher m = pattern.matcher(paramName);
            if (m.find()) {
                row_number = Integer.parseInt(m.group(1));
                place_number = Integer.parseInt(request.getParameter(paramName));
                //out.println(row_number + " " + place_number);
                PlaceServiceImpl.getInstance().save(new PlaceDTO(row_number, place_number, hallDTO));
            }
        }
        response.sendRedirect(request.getContextPath() + "/pages/admin/admin.jsp");
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request,response);
    }
}
