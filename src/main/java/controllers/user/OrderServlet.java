package controllers.user;

import dto.MovieSessionDTO;
import dto.PlaceDTO;
import dto.TicketDTO;
import dto.UserDTO;
import service.impl.MovieSessionServiceImpl;
import service.impl.PlaceServiceImpl;
import service.impl.TicketServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@WebServlet(name = "OrderServlet", urlPatterns={"/orders"})
public class OrderServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        UserDTO userDTO = (UserDTO) request.getSession().getAttribute("user");
        if (userDTO != null && userDTO.getRole().getName().equalsIgnoreCase("user")) {
           // request.setAttribute("user", userDTO);
            if(request.getParameterNames().hasMoreElements()){
            if (request.getParameter("method").contains("so")) {
                MovieSessionDTO movieSessionDTO = MovieSessionServiceImpl.getInstance().getById(Integer.parseInt(request.getParameter("movieSessionID")));

                String order = request.getParameter("order");
                Integer row_id, place_number;
                Map<Integer, Integer> integerMap = new HashMap<>();
                Pattern pattern = Pattern.compile(";;(\\d+);(\\d+)");
                Matcher m = pattern.matcher(order);
                while (m.find()) {
                    row_id = Integer.parseInt(m.group(1));
                    PlaceDTO placeDTO = PlaceServiceImpl.getInstance().getById(row_id);
                    place_number = Integer.parseInt(m.group(2));
                    TicketServiceImpl.getInstance().save(new TicketDTO(new Date(System.currentTimeMillis()), 1, 50.0, movieSessionDTO, placeDTO, userDTO, place_number));
                }

                request.getRequestDispatcher("/pages/user/orders.jsp").forward(request,response);
            }
            }
                List<TicketDTO> ticketDTOList = TicketServiceImpl.getInstance().getAll_ByUser(userDTO.getId());
                ticketDTOList.sort(new Comparator<TicketDTO>() {
                    @Override
                    public int compare(TicketDTO ticketDTO, TicketDTO t1) {
                        return ticketDTO.getDate().compareTo(t1.getDate());
                    }
                });
                request.setAttribute("ticketDTOList", ticketDTOList);

                DateTimeFormatter FORMAT = DateTimeFormatter.ofPattern("yyyy-MM-dd");
                request.setAttribute("FORMAT", FORMAT);
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                request.setAttribute("sdf", sdf);


                request.getRequestDispatcher("pages/user/orders.jsp").forward(request, response);

        }else{
            request.getRequestDispatcher("/pages/common/login.jsp").forward(request, response);
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request,response);
    }
}
