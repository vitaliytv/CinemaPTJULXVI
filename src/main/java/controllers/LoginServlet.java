package controllers;

import dto.UserDTO;
import service.impl.UserServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


@WebServlet(name = "LoginServlet", urlPatterns={"/login"})
public class LoginServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        if(request.getParameter("submit").contains("Registration")){
            response.sendRedirect(request.getContextPath() + "/pages/common/registration.jsp");
        } else{
            String login = request.getParameter("login");
            String password = request.getParameter("password");
            UserDTO userDTO = UserServiceImpl.getInstance().getByLogin(login);

            if(userDTO != null && userDTO.getPassword().equals(password) && userDTO.getRole().getName().equalsIgnoreCase("user")){
                request.getSession().setAttribute("user", userDTO);
                request.getSession().setAttribute("id", userDTO.getId());
                response.sendRedirect(request.getContextPath() + "/");
                //response.sendRedirect(request.getSession().getAttribute("url").toString());
            } else if(userDTO != null && userDTO.getPassword().equals(password) && userDTO.getRole().getName().equalsIgnoreCase("Admin")) {
                request.getSession().setAttribute("user", userDTO);
                response.sendRedirect(request.getContextPath() + "/pages/admin/admin.jsp");
            } else{
                request.getSession().setAttribute("message", "Неправильно набран login или пароль!");
                response.sendRedirect(request.getContextPath() + "/pages/common/login.jsp");
            }
        }



    }
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
            doPost(request, response);
    }
}
