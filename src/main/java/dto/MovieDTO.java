package dto;

import model.Entity;

import java.time.LocalDate;

public class MovieDTO extends Entity<Integer> {

    private String name;
    private int year;
    private String country;
    private String genre;
    private String starring;
    private String director;
    private int length;
    private LocalDate rentStart;
    private LocalDate rentEnd;
    private String description;
    private String ageLimit;
    private String image;
    private String smallImage;

    public MovieDTO() {
    }

    public MovieDTO(String name, int year, String country, String genre, String starring, String director, int length, LocalDate rentStart, LocalDate rentEnd, String description, String ageLimit, String image, String smallImage) {
        this.name = name;
        this.year = year;
        this.country = country;
        this.genre = genre;
        this.starring = starring;
        this.director = director;
        this.length = length;
        this.rentStart = rentStart;
        this.rentEnd = rentEnd;
        this.description = description;
        this.ageLimit = ageLimit;
        this.image = image;
        this.smallImage = smallImage;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getSmallImage() {
        return smallImage;
    }

    public void setSmallImage(String smallImage) {
        this.smallImage = smallImage;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public String getStarring() {
        return starring;
    }

    public void setStarring(String starring) {
        this.starring = starring;
    }

    public String getDirector() {
        return director;
    }

    public void setDirector(String director) {
        this.director = director;
    }

    public int getLength() {
        return length;
    }

    public void setLength(int length) {
        this.length = length;
    }

    public LocalDate getRentStart() {
        return rentStart;
    }

    public void setRentStart(LocalDate rentStart) {
        this.rentStart = rentStart;
    }

    public LocalDate getRentEnd() {
        return rentEnd;
    }

    public void setRentEnd(LocalDate rentEnd) {
        this.rentEnd = rentEnd;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getAgeLimit() {
        return ageLimit;
    }

    public void setAgeLimit(String ageLimit) {
        this.ageLimit = ageLimit;
    }

    @Override
    public String toString() {
        return "MovieDTO{" +
                "name='" + name + '\'' +
                ", year=" + year +
                ", country='" + country + '\'' +
                ", genre='" + genre + '\'' +
                ", starring='" + starring + '\'' +
                ", director='" + director + '\'' +
                ", length=" + length +
                ", rentStart=" + rentStart +
                ", rentEnd=" + rentEnd +
                ", description='" + description + '\'' +
                ", ageLimit='" + ageLimit + '\'' +
                ", image='" + image + '\'' +
                ", smallImage='" + smallImage + '\'' + super.toString() +
                '}';
    }
}