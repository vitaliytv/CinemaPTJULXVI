package dto;

import model.Entity;

import java.util.Date;

/**
 * Class TicketDTO
 */
public class TicketDTO extends Entity<Integer> {
    private Date date;
    private int ticketCondition; //(0-free, 1-sold, 2-booked)
    private double price;
    private MovieSessionDTO movieSession;
    private PlaceDTO place;
    private UserDTO user;
    private int placeNumber;

    public TicketDTO() {
    }

    public TicketDTO(Date date, int ticketCondition, double price, MovieSessionDTO movieSession, PlaceDTO place, UserDTO user, int placeNumber) {
        this.date = date;
        this.ticketCondition = ticketCondition;
        this.price = price;
        this.movieSession = movieSession;
        this.place = place;
        this.user = user;
        this.placeNumber = placeNumber;
    }

    public int getPlaceNumber() {
        return placeNumber;
    }

    public void setPlaceNumber(int placeNumber) {
        this.placeNumber = placeNumber;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public int getTicketCondition() {
        return ticketCondition;
    }

    public void setTicketCondition(int ticketCondition) {
        this.ticketCondition = ticketCondition;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public MovieSessionDTO getMovieSession() {
        return movieSession;
    }

    public void setMovieSession(MovieSessionDTO movieSession) {
        this.movieSession = movieSession;
    }

    public PlaceDTO getPlace() {
        return place;
    }

    public void setPlace(PlaceDTO place) {
        this.place = place;
    }

    public UserDTO getUser() {
        return user;
    }

    public void setUser(UserDTO user) {
        this.user = user;
    }

    @Override
    public String toString() {
        return "TicketDTO{" +
                "date=" + date +
                ", ticketCondition=" + ticketCondition +
                ", price=" + price +
                ", movieSession=" + movieSession +
                ", place=" + place +
                ", user=" + user +
                '}' + super.toString();
    }
}
