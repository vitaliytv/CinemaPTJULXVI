package dto;

import model.Entity;

/**
 * Class HallDTO
 */
public class HallDTO extends Entity<Integer> {
    private String name;
    private String description;

    public HallDTO() {
    }

    public HallDTO(String name, String description) {
        this.name = name;
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "HallDTO{" +
                "name='" + name + '\'' +
                ", description='" + description + '\'' +
                '}' + super.toString();
    }
}
