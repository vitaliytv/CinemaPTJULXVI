package dto;

import model.Entity;

/**
 * Class PlaceDTO
 */
public class PlaceDTO extends Entity<Integer> {
    private int rowNumber;
    private int placeNumber;
    private HallDTO hall;

    public PlaceDTO() {
    }

    public PlaceDTO(int rowNumber, int placeNumber, HallDTO hall) {
        this.rowNumber = rowNumber;
        this.placeNumber = placeNumber;
        this.hall = hall;
    }

    public int getRowNumber() {
        return rowNumber;
    }

    public void setRowNumber(int rowNumber) {
        this.rowNumber = rowNumber;
    }

    public int getPlaceNumber() {
        return placeNumber;
    }

    public void setPlaceNumber(int placeNumber) {
        this.placeNumber = placeNumber;
    }

    public HallDTO getHall() {
        return hall;
    }

    public void setHall(HallDTO hall) {
        this.hall = hall;
    }

    @Override
    public String toString() {
        return "PlaceDTO{" +
                "rowNumber=" + rowNumber +
                ", placeNumber=" + placeNumber +
                ", hall=" + hall +
                '}' + super.toString();
    }
}
