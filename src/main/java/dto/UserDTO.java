package dto;

import model.Entity;

import java.time.LocalDate;

/**
 * Class UserDTO
 */
public class UserDTO extends Entity<Integer> {

    private String name;
    private String surname;
    private String email;
    private String login;
    private String password;
    private int phone;
    private LocalDate birthday;
    private String sex;
    private RoleDTO role;

    public UserDTO() {
    }

    public UserDTO(String name, String surname, String email, String login, String password, int phone, LocalDate birthday, String sex, RoleDTO role) {
        this.name = name;
        this.surname = surname;
        this.email = email;
        this.login = login;
        this.password = password;
        this.phone = phone;
        this.birthday = birthday;
        this.sex = sex;
        this.role = role;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getPhone() {
        return phone;
    }

    public void setPhone(int phone) {
        this.phone = phone;
    }

    public LocalDate getBirthday() {
        return birthday;
    }

    public void setBirthday(LocalDate birthday) {
        this.birthday = birthday;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public RoleDTO getRole() {
        return role;
    }

    public void setRole(RoleDTO role) {
        this.role = role;
    }

    @Override
    public String toString() {
        return "UserDTO{" +
                "name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", email='" + email + '\'' +
                ", login='" + login + '\'' +
                ", password='" + password + '\'' +
                ", phone=" + phone +
                ", birthday=" + birthday +
                ", sex='" + sex + '\'' +
                ", role=" + role +
                '}' + super.toString();
    }
}


