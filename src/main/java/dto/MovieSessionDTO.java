package dto;

import model.Entity;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Class MovieSessionDTO
 */
public class MovieSessionDTO extends Entity<Integer> {
    private String name;
    private LocalDateTime date;
    private MovieDTO movie;
    private HallDTO hall;

    public MovieSessionDTO() {
    }

    public MovieSessionDTO(String name, LocalDateTime date, MovieDTO movie, HallDTO hall) {
        this.name = name;
        this.date = date;
        this.movie = movie;
        this.hall = hall;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LocalDateTime getDate() {
        return date;
    }

    public void setDate(LocalDateTime date) {
        this.date = date;
    }

    public MovieDTO getMovie() {
        return movie;
    }

    public void setMovie(MovieDTO movie) {
        this.movie = movie;
    }

    public HallDTO getHall() {
        return hall;
    }

    public void setHall(HallDTO hall) {
        this.hall = hall;
    }

    @Override
    public String toString() {
        return "MovieSessionDTO{" +
                "name='" + name + '\'' +
                ", date=" + date +
                ", movie=" + movie +
                ", hall=" + hall +
                '}' + super.toString();
    }
}
