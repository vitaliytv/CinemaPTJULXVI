package service.impl;

import dao.DaoFactory;
import dao.api.Dao;
import dto.TicketDTO;
import mapper.BeanMapper;
import model.Ticket;
import service.api.Service;

import java.util.List;

public class TicketServiceImpl implements Service<Integer, TicketDTO>{
    private static TicketServiceImpl service;
    private Dao<Integer, Ticket> ticketDao;
    private BeanMapper beanMapper;

    private TicketServiceImpl(){
        ticketDao = DaoFactory.getInstance().getTicketDao();
        beanMapper = BeanMapper.getInstance();
    }

    public static synchronized TicketServiceImpl getInstance(){
        if(service == null){
            service = new TicketServiceImpl();
        }
        return service;
    }

    @Override
    public List<TicketDTO> getAll() {
        List<Ticket> tickets = ticketDao.getAll();
        List<TicketDTO> ticketDTOs = beanMapper.listMapToList(tickets, TicketDTO.class);
        return ticketDTOs;
    }

    @Override
    public TicketDTO getById(Integer id) {
        Ticket ticket = ticketDao.getById(id);
        TicketDTO ticketDTO = beanMapper.singleMapper(ticket, TicketDTO.class);
        return ticketDTO;
    }

    @Override
    public void save(TicketDTO entity) {
        Ticket ticket = beanMapper.singleMapper(entity, Ticket.class);
        ticketDao.save(ticket);
    }

    public List<TicketDTO> getAll_ByMovieSession(Integer value){
        List<Ticket> ticketList = ticketDao.getBy_All("moviesession_id", value);
        List<TicketDTO> ticketDTOs = beanMapper.listMapToList(ticketList, TicketDTO.class);
        return ticketDTOs;
    }

    public List<TicketDTO> getAll_ByUser(Integer value){
        List<Ticket> ticketList = ticketDao.getBy_All("user_id", value);
        List<TicketDTO> ticketDTOs = beanMapper.listMapToList(ticketList, TicketDTO.class);
        return ticketDTOs;
    }

    @Override
    public void delete(Integer key) {

    }

    @Override
    public void update(TicketDTO entity) {

    }
}
