package service.impl;

import dao.DaoFactory;
import dao.api.Dao;
import dto.PlaceDTO;
import mapper.BeanMapper;
import model.Place;
import service.api.Service;

import java.util.List;


public class PlaceServiceImpl implements Service<Integer, PlaceDTO> {

    private static PlaceServiceImpl service;
    private Dao<Integer, Place> placeDao;
    private BeanMapper beanMapper;

    private PlaceServiceImpl(){
        placeDao = DaoFactory.getInstance().getPlaceDao();
        beanMapper = BeanMapper.getInstance();
    }

    public static synchronized PlaceServiceImpl getInstance(){
        if(service == null){
            service = new PlaceServiceImpl();
        }
        return service;
    }

    @Override
    public List<PlaceDTO> getAll() {
        List<Place> place = placeDao.getAll();
        List<PlaceDTO> placeDTOs = beanMapper.listMapToList(place, PlaceDTO.class);
        return placeDTOs;
    }

    @Override
    public PlaceDTO getById(Integer id) {
        Place place = placeDao.getById(id);
        PlaceDTO placeDTO = beanMapper.singleMapper(place, PlaceDTO.class);
        return placeDTO;
    }

    @Override
    public void save(PlaceDTO placeDTO) {
        Place place = beanMapper.singleMapper(placeDTO, Place.class);
        placeDao.save(place);

    }

    public List<PlaceDTO> getBy_All(Integer value){
        List<Place> placeList = placeDao.getBy_All("hall_id", value);
        List<PlaceDTO> placeDTOs = beanMapper.listMapToList(placeList, PlaceDTO.class);
        return placeDTOs;
    }

    @Override
    public void delete(Integer id) {
        placeDao.delete(id);
    }

    @Override
    public void update(PlaceDTO placeDTO) {
        Place place = beanMapper.singleMapper(placeDTO, Place.class);
        placeDao.update(place);
    }
}
