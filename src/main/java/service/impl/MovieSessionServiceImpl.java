package service.impl;

import dao.DaoFactory;
import dao.api.Dao;
import dto.MovieSessionDTO;
import mapper.BeanMapper;
import model.MovieSession;
import service.api.Service;

import java.time.LocalDateTime;
import java.util.List;


public class MovieSessionServiceImpl implements Service<Integer, MovieSessionDTO> {
    private static MovieSessionServiceImpl service;
    private Dao<Integer, MovieSession> movieSessionDao;
    private BeanMapper beanMapper;

    private MovieSessionServiceImpl() {
        movieSessionDao = DaoFactory.getInstance().getMovieSessionDao();
        beanMapper = BeanMapper.getInstance();
    }

    public static synchronized MovieSessionServiceImpl getInstance() {
        if (service == null) {
            service = new MovieSessionServiceImpl();
        }
        return service;
    }
    @Override
    public List<MovieSessionDTO> getAll() {
        List<MovieSession> movieSessions = movieSessionDao.getAll();
        List<MovieSessionDTO> movieSessionDTOs = beanMapper.listMapToList(movieSessions, MovieSessionDTO.class);
        return movieSessionDTOs;
    }

    @Override
    public MovieSessionDTO getById(Integer id) {
        MovieSession movieSession = movieSessionDao.getById(id);
        MovieSessionDTO movieSessionDTO = beanMapper.singleMapper(movieSession, MovieSessionDTO.class);
        return movieSessionDTO;
    }
    public List<MovieSessionDTO> getByCurrentDate(LocalDateTime date){
        List<MovieSession> movieSessions = movieSessionDao.getAllWhere("date", date);
        List<MovieSessionDTO> movieSessionDTOs = beanMapper.listMapToList(movieSessions, MovieSessionDTO.class);
        return movieSessionDTOs;
    }

    @Override
    public void save(MovieSessionDTO entity) {
        MovieSession movieSession = beanMapper.singleMapper(entity, MovieSession.class);
        movieSessionDao.save(movieSession);
    }

    @Override
    public void delete(Integer id) {
        movieSessionDao.delete(id);
    }

    @Override
    public void update(MovieSessionDTO movieSessionDTO) {
        MovieSession movieSession = beanMapper.singleMapper(movieSessionDTO, MovieSession.class);
        movieSessionDao.update(movieSession);
    }
}
