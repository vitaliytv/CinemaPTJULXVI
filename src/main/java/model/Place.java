package model;

/**
 * Created by Vitaliy on 27.09.2016.
 */
public class Place extends Entity<Integer> {
    private int rowNumber;
    private int placeNumber;
    private Hall hall;

    public Place() {
    }

    public Place(int rowNumber, int placeNumber, Hall hall) {
        this.rowNumber = rowNumber;
        this.placeNumber = placeNumber;
        this.hall = hall;
    }

    public int getRowNumber() {
        return rowNumber;
    }

    public void setRowNumber(int rowNumber) {
        this.rowNumber = rowNumber;
    }

    public int getPlaceNumber() {
        return placeNumber;
    }

    public void setPlaceNumber(int placeNumber) {
        this.placeNumber = placeNumber;
    }

    public Hall getHall() {
        return hall;
    }

    public void setHall(Hall hall) {
        this.hall = hall;
    }

    @Override
    public String toString() {
        return "Place{" +
                "rowNumber=" + rowNumber +
                ", placeNumber=" + placeNumber +
                ", hall=" + hall +
                '}' + super.toString();
    }


}
