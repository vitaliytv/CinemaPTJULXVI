package model;


import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;

/**
 * Class MovieSession
 */
public class MovieSession extends Entity<Integer> {
    private String name;
    private LocalDateTime date;
    private Movie movie;
    private Hall hall;

    public MovieSession() {
    }

    public MovieSession(String name, LocalDateTime date, Movie movie, Hall hall) {
        this.name = name;
        this.date = date;
        this.movie = movie;
        this.hall = hall;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LocalDateTime getDate() {
        return date;
    }

    public void setDate(LocalDateTime date) {
        this.date = date;
    }

    public Movie getMovie() {
        return movie;
    }

    public void setMovie(Movie movie) {
        this.movie = movie;
    }

    public Hall getHall() {
        return hall;
    }

    public void setHall(Hall hall) {
        this.hall = hall;
    }

    @Override
    public String toString() {
        return "MovieSession{" +
                "name='" + name + '\'' +
                ", date=" + date +
                ", movie=" + movie +
                ", hall=" + hall +
                '}' + super.toString();
    }
}
