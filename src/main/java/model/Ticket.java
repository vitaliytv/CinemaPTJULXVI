package model;


import java.util.Date;

/**
 * Class Ticket
 */
public class Ticket extends Entity<Integer> {
    private Date date;
    private int ticketCondition; //(0-free, 1-sold, 2-booked)
    private double price;
    private MovieSession movieSession;
    private Place place;
    private User user;
    private int placeNumber;
    //private int place;
    //private int row;


    public Ticket() {
    }

    public Ticket(Date date, int ticketCondition, double price, MovieSession movieSession, Place place, User user, int placeNumber) {
        this.date = date;
        this.ticketCondition = ticketCondition;
        this.price = price;
        this.movieSession = movieSession;
        this.place = place;
        this.user = user;
        this.placeNumber = placeNumber;
    }

    public int getPlaceNumber() {
        return placeNumber;
    }

    public void setPlaceNumber(int placeNumber) {
        this.placeNumber = placeNumber;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public int getTicketCondition() {
        return ticketCondition;
    }

    public void setTicketCondition(int ticketCondition) {
        this.ticketCondition = ticketCondition;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public MovieSession getMovieSession() {
        return movieSession;
    }

    public void setMovieSession(MovieSession movieSession) {
        this.movieSession = movieSession;
    }

    public Place getPlace() {
        return place;
    }

    public void setPlace(Place place) {
        this.place = place;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @Override
    public String toString() {
        return "Ticket{" +
                "date=" + date +
                ", ticketCondition=" + ticketCondition +
                ", price=" + price +
                ", movieSession=" + movieSession +
                ", place=" + place +
                ", user=" + user +
                '}' + super.toString();
    }
}
