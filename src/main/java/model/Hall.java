package model;


import java.util.Arrays;

/**
 * Class Hall
 */
public class Hall extends Entity<Integer> {
    private String name;
   // private int numberOfPlaces;
    private String description;
  //  private int numberOfRow;
   // private int[] rowsAndPlaces;

    public Hall(String name, String description) {
        this.name = name;
        this.description = description;
    }
/*
    public Hall(String name, String description, int[] rowsAndPlaces) {
        setName(name);
        setDescription(description);
        //setRowsAndPlaces(rowsAndPlaces);
        /*for (int i = 0; i < rowsAndPlaces.length; i++) {
            numberOfPlaces += ((i+1)*rowsAndPlaces[i]);
            numberOfRow += i+1;
        }
    }
*/

    public Hall() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }



    @Override
    public String toString() {
        return "Hall{" +
                "name='" + name + '\'' +
                ", description='" + description + '\'' +
                '}' + super.toString();
    }
}
