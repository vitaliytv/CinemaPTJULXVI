package model;


import java.net.URL;
import java.time.LocalDate;

/**
 * Class Movie
 */
public class Movie extends Entity<Integer> {
    private String name;
    private int year;
    private String country;
    private String genre;
    private String starring;
    private String director;
    private int length;
    private LocalDate rentStart;
    private LocalDate rentEnd;
    private String description;
    private String ageLimit;
    private String image;
    private String smallImage;
   // SimpleDateFormat timeFormat = new SimpleDateFormat("dd.MM.YYYY");

    public Movie() {
    }

    public Movie(String name, int year, String country, String genre, String starring, String director, int length, LocalDate rentStart, LocalDate rentEnd, String description, String ageLimit, String image, String smallImage) {
        this.name = name;
        this.year = year;
        this.country = country;
        this.genre = genre;
        this.starring = starring;
        this.director = director;
        this.length = length;
        this.rentStart = rentStart;
        this.rentEnd = rentEnd;
        this.description = description;
        this.ageLimit = ageLimit;
        this.image = image;
        this.smallImage = smallImage;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getSmallImage() {
        return smallImage;
    }

    public void setSmallImage(String smallImage) {
        this.smallImage = smallImage;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public String getStarring() {
        return starring;
    }

    public void setStarring(String starring) {
        this.starring = starring;
    }

    public String getDirector() {
        return director;
    }

    public void setDirector(String director) {
        this.director = director;
    }

    public int getLength() {
        return length;
    }

    public void setLength(int length) {
        this.length = length;
    }

    public LocalDate getRentStart() {
        return rentStart;
    }

    public void setRentStart(LocalDate rentStart) {
        this.rentStart = rentStart;
    }

    public LocalDate getRentEnd() {
        return rentEnd;
    }

    public void setRentEnd(LocalDate rentEnd) {
        this.rentEnd = rentEnd;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getAgeLimit() {
        return ageLimit;
    }

    public void setAgeLimit(String ageLimit) {
        this.ageLimit = ageLimit;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Movie)) return false;
        if (!super.equals(o)) return false;

        Movie movie = (Movie) o;

        if (getYear() != movie.getYear()) return false;
        if (getLength() != movie.getLength()) return false;
        if (getName() != null ? !getName().equals(movie.getName()) : movie.getName() != null) return false;
        if (getCountry() != null ? !getCountry().equals(movie.getCountry()) : movie.getCountry() != null) return false;
        if (getGenre() != null ? !getGenre().equals(movie.getGenre()) : movie.getGenre() != null) return false;
        if (getStarring() != null ? !getStarring().equals(movie.getStarring()) : movie.getStarring() != null)
            return false;
        if (getDirector() != null ? !getDirector().equals(movie.getDirector()) : movie.getDirector() != null)
            return false;
        if (getRentStart() != null ? !getRentStart().equals(movie.getRentStart()) : movie.getRentStart() != null)
            return false;
        if (getRentEnd() != null ? !getRentEnd().equals(movie.getRentEnd()) : movie.getRentEnd() != null) return false;
        if (getDescription() != null ? !getDescription().equals(movie.getDescription()) : movie.getDescription() != null)
            return false;
        return getAgeLimit() != null ? getAgeLimit().equals(movie.getAgeLimit()) : movie.getAgeLimit() == null;

    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (getName() != null ? getName().hashCode() : 0);
        result = 31 * result + getYear();
        result = 31 * result + (getCountry() != null ? getCountry().hashCode() : 0);
        result = 31 * result + (getGenre() != null ? getGenre().hashCode() : 0);
        result = 31 * result + (getStarring() != null ? getStarring().hashCode() : 0);
        result = 31 * result + (getDirector() != null ? getDirector().hashCode() : 0);
        result = 31 * result + getLength();
        result = 31 * result + (getRentStart() != null ? getRentStart().hashCode() : 0);
        result = 31 * result + (getRentEnd() != null ? getRentEnd().hashCode() : 0);
        result = 31 * result + (getDescription() != null ? getDescription().hashCode() : 0);
        result = 31 * result + (getAgeLimit() != null ? getAgeLimit().hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Movie{" +
                "name='" + name + '\'' +
                ", year=" + year +
                ", country='" + country + '\'' +
                ", genre='" + genre + '\'' +
                ", starring='" + starring + '\'' +
                ", director='" + director + '\'' +
                ", length=" + length +
                ", rentStart=" + rentStart +
                ", rentEnd=" + rentEnd +
                ", description='" + description + '\'' +
                ", ageLimit='" + ageLimit + '\'' +
                ", image='" + image + '\'' +
                ", smallImage='" + smallImage + '\'' + super.toString() +
                '}';
    }
}
