package dao;

/**
 * Class SQLs
 */
public class SQLs {
    public static final String SELECT_ALL = "Select * from %s";
    public static final String FIND_BY_ID = "Select * from %s where id = ?";
    public static final String FIND_BY = "Select * from %s where %s = ?";
    public static final String FIND_BY_PLUS = "Select * from %s where %s > ?";

    public static final String DELETE_BY_ID = "DELETE FROM %s WHERE id = ?";

    public static  final String INSERT_HALL = "Insert into hall (name, description) values (?,?)";
    public static  final String UPDATE_HALL = "UPDATE hall SET name = ?, description = ? WHERE id = ?";

    public static  final String INSERT_MOVIE = "Insert into movie (name, year, country, genre, starring, director, length, rent_start, rent_end, description, age_limit, image, small_image) values (?,?,?,?,?,?,?,?,?,?,?,?,?)";
    public static  final String UPDATE_MOVIE = "UPDATE movie SET name = ?, year = ?, country = ?, genre = ?, starring = ?, director = ?, length = ?, rent_start = ?, rent_end = ?, description = ?, age_limit = ?, image = ?, small_image = ? WHERE id = ?";

    public static  final String INSERT_MOVIE_SESSION = "Insert into moviesession (name, date, movie_id, hall_id) values (?,?,?,?)";
    public static  final String UPDATE_MOVIE_SESSION = "UPDATE moviesession SET name = ?, date = ?, movie_id = ?, hall_id = ? WHERE id = ?";

    public static  final String INSERT_PLACE = "Insert into place (row_number, place_number, hall_id) values (?,?,?)";
    public static  final String UPDATE_PLACE = "UPDATE place SET row_number = ?, place_number = ?, hall_id = ? WHERE id = ?";

    public static  final String INSERT_TICKET = "Insert into ticket (date, ticket_condition, price, moviesession_id, place_id, user_id, place_number) values (?,?,?,?,?,?,?)";
    public static  final String UPDATE_TICKET = "UPDATE ticket SET date = ?, ticket_condition = ?, price = ?, moviesession_id = ?, place_id = ?, user_id = ?, place_number = ? WHERE id = ?";

    public static  final String INSERT_USER = "Insert into user (name, surname, email, login, password, phone, birthday, sex, role_id) values (?,?,?,?,?,?,?,?,?)";
    public static  final String UPDATE_USER = "UPDATE user SET name = ?, surname = ?, email = ?, login = ?, password = ?, phone = ?, birthday = ?, sex = ?, role_id = ? WHERE id = ?";


}
