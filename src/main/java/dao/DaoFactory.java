package dao;

import dao.api.Dao;
import dao.impl.*;
import helpers.PropertyHolder;
import model.*;

/**
 * Class DaoFactory
 */
public class DaoFactory {
    private static DaoFactory instance = null;

    private Dao<Integer, Hall> hallDao;
    private Dao<Integer, Movie> movieDao;
    private Dao<Integer, MovieSession> movieSessionDao;
    private Dao<Integer, Place> placeDao;
    private Dao<Integer, Role> roleDao;
    private Dao<Integer, Ticket> ticketDao;
    private Dao<Integer, User> userDao;

    private DaoFactory(){
        loadDaos();
    }



    public static DaoFactory getInstance(){
        if(instance == null){
            instance = new DaoFactory();
        }
        return instance;
    }

    private void loadDaos() {
       if(PropertyHolder.getInstance().isInMemoryDB()){
            //inmemory daos
       }else{
           //jdbc daos
           hallDao = new HallDaoImpl();
           movieDao = new MovieDaoImpl();
           movieSessionDao = new MovieSessionDaoImpl();
           placeDao = new PlaceDaoImpl();
           roleDao = new RoleDaoImpl();
           ticketDao = new TicketDaoImpl();
           userDao = new UserDaoImpl();
       }
    }

    public static void setInstance(DaoFactory instance) {
        DaoFactory.instance = instance;
    }

    public Dao<Integer, Hall> getHallDao() {
        return hallDao;
    }

    public void setHallDao(Dao<Integer, Hall> hallDao) {
        this.hallDao = hallDao;
    }

    public Dao<Integer, Movie> getMovieDao() {
        return movieDao;
    }

    public void setMovieDao(Dao<Integer, Movie> movieDao) {
        this.movieDao = movieDao;
    }

    public Dao<Integer, MovieSession> getMovieSessionDao() {
        return movieSessionDao;
    }

    public void setMovieSessionDao(Dao<Integer, MovieSession> movieSessionDao) {
        this.movieSessionDao = movieSessionDao;
    }

    public Dao<Integer, Place> getPlaceDao() {
        return placeDao;
    }

    public void setPlaceDao(Dao<Integer, Place> placeDao) {
        this.placeDao = placeDao;
    }

    public Dao<Integer, Role> getRoleDao() {
        return roleDao;
    }

    public void setRoleDao(Dao<Integer, Role> roleDao) {
        this.roleDao = roleDao;
    }

    public Dao<Integer, Ticket> getTicketDao() {
        return ticketDao;
    }

    public void setTicketDao(Dao<Integer, Ticket> ticketDao) {
        this.ticketDao = ticketDao;
    }

    public Dao<Integer, User> getUserDao() {
        return userDao;
    }

    public void setUserDao(Dao<Integer, User> userDao) {
        this.userDao = userDao;
    }
}
