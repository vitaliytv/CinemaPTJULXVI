package dao.impl;


import model.Movie;

import java.net.URL;
import java.sql.*;
import java.util.LinkedList;
import java.util.List;

import static dao.SQLs.INSERT_MOVIE;
import static dao.SQLs.UPDATE_MOVIE;

/**
 * Class MovieDaoImpl
 */
public final class MovieDaoImpl extends CrudDAO<Movie> {

    private static MovieDaoImpl crudDAO;

    public MovieDaoImpl() {
        super(Movie.class);
    }

    private void setStatement(Movie entity, PreparedStatement preparedStatement) throws SQLException {
        preparedStatement.setString(1, entity.getName());
        preparedStatement.setInt(2, entity.getYear());
        preparedStatement.setString(3, entity.getCountry());
        preparedStatement.setString(4, entity.getGenre());
        preparedStatement.setString(5, entity.getStarring());
        preparedStatement.setString(6, entity.getDirector());
        preparedStatement.setInt(7, entity.getLength());
        preparedStatement.setDate(8, Date.valueOf(entity.getRentStart()));
        preparedStatement.setDate(9, Date.valueOf(entity.getRentEnd()));
        preparedStatement.setString(10, entity.getDescription());
        preparedStatement.setString(11, entity.getAgeLimit());
        preparedStatement.setString(12, entity.getImage());
        preparedStatement.setString(13, entity.getSmallImage());
    }

    @Override
    protected PreparedStatement createUpdateStatement(Connection connection, Movie entity) throws SQLException {
        PreparedStatement preparedStatement = connection.prepareStatement(UPDATE_MOVIE);
        preparedStatement.setInt(14,entity.getId());
        setStatement(entity, preparedStatement);
        return preparedStatement;
    }

    @Override
    public PreparedStatement createInsertStatement(Connection connection, Movie entity) throws SQLException {
        PreparedStatement preparedStatement = connection.prepareStatement(INSERT_MOVIE, Statement.RETURN_GENERATED_KEYS);
        setStatement(entity, preparedStatement);
        return preparedStatement;
    }

    @Override
    public List<Movie> readAll(ResultSet resultSet) throws SQLException {
        List<Movie> result = new LinkedList<>();
        Movie movie = null;
        while (resultSet.next()) {
            movie = new Movie();
            movie.setId(resultSet.getInt("id"));
            movie.setName(resultSet.getString("name"));
            movie.setYear(resultSet.getInt("year"));
            movie.setCountry(resultSet.getString("country"));
            movie.setGenre(resultSet.getString("genre"));
            movie.setStarring(resultSet.getString("starring"));
            movie.setDirector(resultSet.getString("director"));
            movie.setLength(resultSet.getInt("length"));
            movie.setRentStart(resultSet.getDate("rent_start").toLocalDate());
            movie.setRentEnd(resultSet.getDate("rent_end").toLocalDate());
            movie.setDescription(resultSet.getString("description"));
            movie.setAgeLimit(resultSet.getString("age_limit"));
            movie.setImage(resultSet.getString("image"));
            movie.setSmallImage(resultSet.getString("small_image"));
            result.add(movie);
        }
        return result;
    }

}