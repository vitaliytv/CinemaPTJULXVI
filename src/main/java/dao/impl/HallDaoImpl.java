package dao.impl;

import model.Hall;

import java.sql.*;
import java.util.LinkedList;
import java.util.List;

import static dao.SQLs.INSERT_HALL;
import static dao.SQLs.UPDATE_HALL;

/**
 * Class HallDaoImpl
 */
public class HallDaoImpl extends CrudDAO<Hall>{

    public HallDaoImpl() {
        super(Hall.class);
    }

    private void setStatement(Hall entity, PreparedStatement preparedStatement) throws SQLException {
        preparedStatement.setString(1, entity.getName());
        preparedStatement.setString(2, entity.getDescription());
    }

    @Override
    protected PreparedStatement createUpdateStatement(Connection connection, Hall entity) throws SQLException {
        PreparedStatement preparedStatement = connection.prepareStatement(UPDATE_HALL);
        preparedStatement.setInt(3,entity.getId());
        setStatement(entity, preparedStatement);
        return preparedStatement;
    }

    @Override
    protected PreparedStatement createInsertStatement(Connection connection, Hall entity) throws SQLException {
        PreparedStatement preparedStatement = connection.prepareStatement(INSERT_HALL, Statement.RETURN_GENERATED_KEYS);
        setStatement(entity, preparedStatement);
        return preparedStatement;
    }

    @Override
    protected List<Hall> readAll(ResultSet resultSet) throws SQLException {
        List<Hall> result = new LinkedList<>();
        Hall hall = null;
        while (resultSet.next()) {
            hall = new Hall();
            hall.setId(resultSet.getInt("id"));
            hall.setName(resultSet.getString("name"));
            hall.setDescription(resultSet.getString("description"));
            result.add(hall);
        }
        return result;
    }
}
