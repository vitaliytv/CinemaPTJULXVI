package dao.impl;

import dao.DaoFactory;
import model.Ticket;

import java.sql.*;
import java.util.LinkedList;
import java.util.List;

import static dao.SQLs.INSERT_TICKET;
import static dao.SQLs.UPDATE_TICKET;

/**
 * Created by Vitaliy on 28.09.2016.
 */
public class TicketDaoImpl extends CrudDAO<Ticket>{

    public TicketDaoImpl() {
        super(Ticket.class);
    }

    private void setStatement(Ticket entity, PreparedStatement preparedStatement) throws SQLException {
        preparedStatement.setDate(1, new Date(entity.getDate().getTime()));
        preparedStatement.setInt(2, entity.getTicketCondition());
        preparedStatement.setDouble(3, entity.getPrice());
        preparedStatement.setInt(4, entity.getMovieSession().getId());
        preparedStatement.setInt(5, entity.getPlace().getId());
        preparedStatement.setInt(6, entity.getUser().getId());
        preparedStatement.setInt(7, entity.getPlaceNumber());
    }

    @Override
    protected PreparedStatement createUpdateStatement(Connection connection, Ticket entity) throws SQLException {
        PreparedStatement preparedStatement = connection.prepareStatement(UPDATE_TICKET);
        setStatement(entity, preparedStatement);
        return preparedStatement;
    }

    @Override
    protected PreparedStatement createInsertStatement(Connection connection, Ticket entity) throws SQLException {
        PreparedStatement preparedStatement = connection.prepareStatement(INSERT_TICKET, Statement.RETURN_GENERATED_KEYS);
        setStatement(entity, preparedStatement);
        return preparedStatement;
    }

    @Override
    protected List<Ticket> readAll(ResultSet resultSet) throws SQLException {
        List<Ticket> result = new LinkedList<>();
        Ticket ticket = null;
        while (resultSet.next()) {
            ticket = new Ticket();
            ticket.setId(resultSet.getInt("id"));
            ticket.setDate(resultSet.getDate("date"));
            ticket.setTicketCondition(resultSet.getInt("ticket_condition"));
            ticket.setPrice(resultSet.getDouble("price"));
            ticket.setMovieSession(DaoFactory.getInstance().getMovieSessionDao().getById(resultSet.getInt("moviesession_id")));
            ticket.setPlace(DaoFactory.getInstance().getPlaceDao().getById(resultSet.getInt("place_id")));
            ticket.setUser(DaoFactory.getInstance().getUserDao().getById(resultSet.getInt("user_id")));
            ticket.setPlaceNumber(resultSet.getInt("place_number"));
            result.add(ticket);
        }
        return result;
    }
}
