package dao.impl;

import dao.DaoFactory;
import model.User;

import java.sql.*;
import java.util.LinkedList;
import java.util.List;

import static dao.SQLs.INSERT_USER;
import static dao.SQLs.UPDATE_USER;

/**
 * Class UserDaoImpl
 */
public class UserDaoImpl extends CrudDAO<User> {
    public UserDaoImpl() {
        super(User.class);
    }

    private void setStatement(User entity, PreparedStatement preparedStatement) throws SQLException {
        preparedStatement.setString(1, entity.getName());
        preparedStatement.setString(2, entity.getSurname());
        preparedStatement.setString(3, entity.getEmail());
        preparedStatement.setString(4, entity.getLogin());
        preparedStatement.setString(5, entity.getPassword());
        preparedStatement.setInt(6, entity.getPhone());
        preparedStatement.setDate(7, Date.valueOf(entity.getBirthday()));
        preparedStatement.setString(8, entity.getSex());
        preparedStatement.setInt(9, entity.getRole().getId());
    }

    @Override
    protected PreparedStatement createUpdateStatement(Connection connection, User entity) throws SQLException {
        PreparedStatement preparedStatement = connection.prepareStatement(UPDATE_USER);
        setStatement(entity, preparedStatement);
        return preparedStatement;
    }

    @Override
    protected PreparedStatement createInsertStatement(Connection connection, User entity) throws SQLException {
        PreparedStatement preparedStatement = connection.prepareStatement(INSERT_USER, Statement.RETURN_GENERATED_KEYS);
        setStatement(entity, preparedStatement);
        return preparedStatement;
    }

    @Override
    protected List<User> readAll(ResultSet resultSet) throws SQLException {
        List<User> result = new LinkedList<>();
        User user = null;
        while (resultSet.next()) {
            user = new User();
            user.setId(resultSet.getInt("id"));
            user.setName(resultSet.getString("name"));
            user.setSurname(resultSet.getString("surname"));
            user.setEmail(resultSet.getString("email"));
            user.setLogin(resultSet.getString("login"));
            user.setPassword(resultSet.getString("password"));
            user.setPhone(resultSet.getInt("phone"));
            user.setBirthday(resultSet.getDate("birthday").toLocalDate());
            user.setSex(resultSet.getString("sex"));
            user.setRole(DaoFactory.getInstance().getRoleDao().getById(resultSet.getInt("role_id")));
            result.add(user);
        }
        return result;
    }
}
