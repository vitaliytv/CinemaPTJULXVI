package dao.impl;

import dao.DaoFactory;
import model.Place;

import java.sql.*;
import java.util.LinkedList;
import java.util.List;

import static dao.SQLs.INSERT_PLACE;
import static dao.SQLs.UPDATE_PLACE;

/**
 * Created by Vitaliy on 28.09.2016.
 */
public class PlaceDaoImpl extends CrudDAO<Place>{

    public PlaceDaoImpl() {
        super(Place.class);
    }

    private void setStatement(Place entity, PreparedStatement preparedStatement) throws SQLException {
        preparedStatement.setInt(1, entity.getRowNumber());
        preparedStatement.setInt(2, entity.getPlaceNumber());
        preparedStatement.setInt(3, entity.getHall().getId());
    }

    @Override
    protected PreparedStatement createUpdateStatement(Connection connection, Place entity) throws SQLException {
        PreparedStatement preparedStatement = connection.prepareStatement(UPDATE_PLACE);
        preparedStatement.setInt(4,entity.getId());
        setStatement(entity, preparedStatement);
        return preparedStatement;
    }

    @Override
    protected PreparedStatement createInsertStatement(Connection connection, Place entity) throws SQLException {
        PreparedStatement preparedStatement = connection.prepareStatement(INSERT_PLACE, Statement.RETURN_GENERATED_KEYS);
        setStatement(entity, preparedStatement);
        return preparedStatement;
    }

    @Override
    protected List<Place> readAll(ResultSet resultSet) throws SQLException {
        List<Place> result = new LinkedList<>();
        Place place = null;
        while (resultSet.next()) {
            place = new Place();
            place.setId(resultSet.getInt("id"));
            place.setRowNumber(resultSet.getInt("row_number"));
            place.setPlaceNumber(resultSet.getInt("place_number"));
            place.setHall(DaoFactory.getInstance().getHallDao().getById(resultSet.getInt("hall_id")));
            result.add(place);
        }
        return result;
    }

}
