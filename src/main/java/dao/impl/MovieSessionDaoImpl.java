package dao.impl;

import dao.DaoFactory;
import model.MovieSession;

import java.sql.*;
import java.util.LinkedList;
import java.util.List;

import static dao.SQLs.INSERT_MOVIE_SESSION;
import static dao.SQLs.UPDATE_MOVIE_SESSION;

/**
 * Created by Vitaliy on 28.09.2016.
 */
public class MovieSessionDaoImpl extends CrudDAO<MovieSession>{

    public MovieSessionDaoImpl() {
        super(MovieSession.class);
    }

    private void setStatement(MovieSession entity, PreparedStatement preparedStatement) throws SQLException {
        preparedStatement.setString(1, entity.getName());
        preparedStatement.setTimestamp(2, Timestamp.valueOf(entity.getDate()));
        preparedStatement.setInt(3, entity.getMovie().getId());
        preparedStatement.setInt(4, entity.getHall().getId());
    }

    @Override
    protected PreparedStatement createUpdateStatement(Connection connection, MovieSession entity) throws SQLException {
        PreparedStatement preparedStatement = connection.prepareStatement(UPDATE_MOVIE_SESSION);
        preparedStatement.setInt(5,entity.getId());
        setStatement(entity, preparedStatement);
        return preparedStatement;
    }

    @Override
    protected PreparedStatement createInsertStatement(Connection connection, MovieSession entity) throws SQLException {
        PreparedStatement preparedStatement = connection.prepareStatement(INSERT_MOVIE_SESSION, Statement.RETURN_GENERATED_KEYS);
        setStatement(entity, preparedStatement);
        return preparedStatement;
    }

    @Override
    protected List<MovieSession> readAll(ResultSet resultSet) throws SQLException {
        List<MovieSession> result = new LinkedList<>();
        MovieSession movieSession = null;
        while (resultSet.next()) {
            movieSession = new MovieSession();
            movieSession.setId(resultSet.getInt("id"));
            movieSession.setName(resultSet.getString("name"));
            movieSession.setDate(resultSet.getTimestamp("date").toLocalDateTime());
            movieSession.setMovie(DaoFactory.getInstance().getMovieDao().getById(resultSet.getInt("movie_id")));
            movieSession.setHall(DaoFactory.getInstance().getHallDao().getById(resultSet.getInt("hall_id")));
            result.add(movieSession);
        }
        return result;
    }
}
