<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<link href="${pageContext.servletContext.contextPath}/resources/css/create_form.css" rel="stylesheet">

<html>
<head>
    <title>Регистрация</title>
</head>
<body>

<script type="text/javascript">
    window.onload = function () {
        document.getElementById("password").onchange = validatePassword;
        document.getElementById("repassword").onchange = validatePassword;
    }
    function validatePassword(){
        var pass2=document.getElementById("repassword").value;
        var pass1=document.getElementById("password").value;
        if(pass1!=pass2)
            document.getElementById("repassword").setCustomValidity("Пароли не идентичны!");
        else
            document.getElementById("repassword").setCustomValidity('');
    }
</script>

<form class="movie_form" name="registrationForm" method="post" action="${pageContext.servletContext.contextPath}/registration">
    <ul>
        <li>
            <h3>Регистрация</h3>
        </li>
        <li>
            <label for="name">Имя:</label>
            <input type="text" name="name" placeholder="Имя" id="name" required/> <br/>
        </li>
        <li>
            <label for="surname">Фамилия:</label>
            <input type="text" name="surname" placeholder="Фамилия" id="surname" required/> <br/>
        </li>
        <li>
            <label for="email">E-mail:</label>
            <input type="email" name="email" placeholder="e-mail"  id="email"required/> <br/>
        </li>
        <li>
            <label for="login">Login:</label>
            <input type="text" name="login" placeholder="login" id="login" required/> <br/>
        </li>
        <li>
            <label for="password">Пароль:</label>
            <input type="password" name="password" placeholder="Пароль" id="password" required/> <br/>
        </li>
        <li>
            <label for="repassword">Повторите пароль:</label>
            <input type="password" name="repassword" placeholder="Повтор пароля" id="repassword" required/> <br/>
        </li>
        <li>
            <label for="phone">Телефон:</label>
            <input type="tel" name="phone" placeholder="Телефон" id="phone" required/> <br/>
        </li>
        <li>
            <label for="birthday">День рождения:</label>
            <input type="date" name="birthday" id="birthday" required/> <br/>
        </li>
        <li>
            <label for="sex">Пол:</label>
            <input class="radio_button" type="radio" name="sex" value="male" />Мужчина<br/>
            <input class="radio_button" type="radio" name="sex" value="female" />Женщина<br/>
        </li>
        <li>
            <button class="submit" type="submit">Сохранить</button>
        </li>
    </ul>
</form>
</div>
</body>
</html>
