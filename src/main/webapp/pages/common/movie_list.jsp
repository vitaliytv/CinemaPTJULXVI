<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<link href="${pageContext.servletContext.contextPath}/resources/css/cinema-style.css" rel="stylesheet">
<link href="${pageContext.servletContext.contextPath}/resources/css/menu.css" rel="stylesheet">


<html>
<head>
    <title>Фильмы</title>
</head>
<body>

<div class="cinema-image"><img width="112" height="88" src="${pageContext.request.contextPath}/resources/image/cinema-logo.jpg"><div class="cinema_name">Кинотеатр "Black and White"</div></div>

<ul class="css-menu-3">
    <li><a href="${pageContext.servletContext.contextPath}/">Главная</a></li>
    <li><a href="${pageContext.servletContext.contextPath}/movieSession">Расписание</a></li>
    <li><a href="${pageContext.servletContext.contextPath}/moviesList">Фильмы</a></li>
    <li><a href="${pageContext.servletContext.contextPath}/cinemaDescription">Кинотеатр</a></li>
    <c:if test="${user != null}">
        <li><a href="${pageContext.servletContext.contextPath}/personalCabinet">Личные данные</a></li>
        <li><a href="${pageContext.servletContext.contextPath}/orders">Заказы</a></li>
        <li><a href="${pageContext.servletContext.contextPath}/logout">Выход</a></li>
    </c:if>
    <c:if test="${user == null}">
        <li><a href="${pageContext.servletContext.contextPath}/pages/common/login.jsp">Вход</a></li>
        <li><a href="${pageContext.servletContext.contextPath}/pages/common/registration.jsp">Регистрация</a></li>
    </c:if>

</ul>

<br />
<h2>Сейчас в прокате</h2>
<c:set var="iter" value="1" />
<table cellpadding="30" cellspacing="20">
    <tr>
        <c:forEach items="${movieDTOList}" var="movies">

            <td align="">
                <a href="${pageContext.servletContext.contextPath}/movie?id=${movies.id}">
                    <div><img src="${pageContext.request.contextPath}${movies.smallImage}" width="160" height="236"></div>
                    <div>${movies.name}</div>

                </a>
                <div class="rent_end">до ${movies.rentEnd.format(FORMAT)}</div>
            </td>
            <c:set var="iter" value="${iter+1}" />
            <c:if test="${iter == 6}">
                <c:set var="iter" value="1" />
                </tr><tr>
            </c:if>
        </c:forEach>
    </tr>
</table>

</body>
</html>
