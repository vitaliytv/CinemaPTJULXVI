<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<link href="${pageContext.servletContext.contextPath}/resources/css/menu.css" rel="stylesheet">
<LINK REL="stylesheet" TYPE="text/css" HREF="${pageContext.servletContext.contextPath}/resources/css/style.css" TITLE="style" />
<link href="${pageContext.servletContext.contextPath}/resources/css/cinema-style.css" rel="stylesheet">
<script src="${pageContext.servletContext.contextPath}/resources/js/jjs.js"></script>

<html>
<head>
    <title>Кинотеатр</title>

</head>
<body>

    <div class="cinema-image"><img width="112" height="88" src="${pageContext.request.contextPath}/resources/image/cinema-logo.jpg"><div class="cinema_name">Кинотеатр "Black and White"</div></div>


<ul class="css-menu-3">
    <li><a href="${pageContext.servletContext.contextPath}/">Главная</a></li>
    <li><a href="${pageContext.servletContext.contextPath}/movieSession">Расписание</a></li>
    <li><a href="${pageContext.servletContext.contextPath}/moviesList">Фильмы</a></li>
    <li><a href="${pageContext.servletContext.contextPath}/cinemaDescription">Кинотеатр</a></li>
    <c:if test="${user != null}">
        <li><a href="${pageContext.servletContext.contextPath}/personalCabinet">Личные данные</a></li>
        <li><a href="${pageContext.servletContext.contextPath}/orders">Заказы</a></li>
        <li><a href="${pageContext.servletContext.contextPath}/logout">Выход</a></li>
    </c:if>
    <c:if test="${user == null}">
        <li><a href="${pageContext.servletContext.contextPath}/pages/common/login.jsp">Вход</a></li>
        <li><a href="${pageContext.servletContext.contextPath}/pages/common/registration.jsp">Регистрация</a></li>
    </c:if>

</ul>

<div class="slider" id="main-slider"><!-- outermost container element -->
    <div class="slider-wrapper"><!-- innermost wrapper element -->
        <c:forEach items="${movieDTOList}" var="movies">
            <a href="${pageContext.servletContext.contextPath}/movie?id=${movies.id}">
                <div class="slide" data-image="${pageContext.request.contextPath}${movies.image}"></div><!-- slides -->
            </a>
        </c:forEach>
    </div>
    <div class="slider-nav"><!-- "Previous" and "Next" actions -->
        <button class="slider-previous">Previous</button>
        <button class="slider-next">Next</button>
    </div>
</div>
</body>
</html>
