<%--
  Created by IntelliJ IDEA.
  User: Vitaliy
  Date: 23.10.2016
  Time: 13:32
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<link href="${pageContext.servletContext.contextPath}/resources/css/menu.css" rel="stylesheet">
<link href="${pageContext.servletContext.contextPath}/resources/css/cinema-style.css" rel="stylesheet">
<link href="${pageContext.servletContext.contextPath}/resources/css/table.css" rel="stylesheet">


<html>
<head>
    <title>Описание кинотеатра</title>

</head>
<body>

<div class="cinema-image"><img width="112" height="88" src="${pageContext.request.contextPath}/resources/image/cinema-logo.jpg"><div class="cinema_name">Кинотеатр "Black and White"</div></div>

<ul class="css-menu-3">
    <li><a href="${pageContext.servletContext.contextPath}/">Главная</a></li>
    <li><a href="${pageContext.servletContext.contextPath}/movieSession">Расписание</a></li>
    <li><a href="${pageContext.servletContext.contextPath}/moviesList">Фильмы</a></li>
    <li><a href="${pageContext.servletContext.contextPath}/cinemaDescription">Кинотеатр</a></li>
    <c:if test="${user != null}">
        <li><a href="${pageContext.servletContext.contextPath}/personalCabinet">Личные данные</a></li>
        <li><a href="${pageContext.servletContext.contextPath}/orders">Заказы</a></li>
        <li><a href="${pageContext.servletContext.contextPath}/logout">Выход</a></li>
    </c:if>
    <c:if test="${user == null}">
        <li><a href="${pageContext.servletContext.contextPath}/pages/common/login.jsp">Вход</a></li>
        <li><a href="${pageContext.servletContext.contextPath}/pages/common/registration.jsp">Регистрация</a></li>
    </c:if>

</ul>


<h2>    Описание КИнотеатра</h2>
<h3>Описание залов</h3>

<table class="table_show" border="2">
<tr>
    <th>
        Название
    </th>
    <th>
        Описание
    </th>
    <th>
        Расположение мест
    </th>

</tr>
<c:forEach items="${hallDTOList}" var="hall">
    <tr>

        <td>
            ${hall.name}
        </td>
        <td>
            ${hall.description}
        </td>
        <td>
            <a href="${pageContext.servletContext.contextPath}/showChoosePlace?method=sh&id=${hall.id}">${hall.name}</a><br/>
        </td>
    </tr>

</c:forEach>
</table>

</body>
</html>
