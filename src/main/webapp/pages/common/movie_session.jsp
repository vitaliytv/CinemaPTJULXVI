<%--
  Created by IntelliJ IDEA.
  User: Vitaliy
  Date: 20.10.2016
  Time: 16:20
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<link href="${pageContext.servletContext.contextPath}/resources/css/cinema-style.css" rel="stylesheet">
<link href="${pageContext.servletContext.contextPath}/resources/css/menu.css" rel="stylesheet">

<html>
<head>
    <title>Расписание</title>
</head>
<body>

<div class="cinema-image"><img width="112" height="88" src="${pageContext.request.contextPath}/resources/image/cinema-logo.jpg"><div class="cinema_name">Кинотеатр "Black and White"</div></div>


<ul class="css-menu-3">
    <li><a href="${pageContext.servletContext.contextPath}/">Главная</a></li>
    <li><a href="${pageContext.servletContext.contextPath}/movieSession">Расписание</a></li>
    <li><a href="${pageContext.servletContext.contextPath}/moviesList">Фильмы</a></li>
    <li><a href="${pageContext.servletContext.contextPath}/cinemaDescription">Кинотеатр</a></li>
    <c:if test="${user != null}">
        <li><a href="${pageContext.servletContext.contextPath}/personalCabinet">Личные данные</a></li>
        <li><a href="${pageContext.servletContext.contextPath}/orders">Заказы</a></li>
        <li><a href="${pageContext.servletContext.contextPath}/logout">Выход</a></li>
    </c:if>
    <c:if test="${user == null}">
        <li><a href="${pageContext.servletContext.contextPath}/pages/common/login.jsp">Вход</a></li>
        <li><a href="${pageContext.servletContext.contextPath}/pages/common/registration.jsp">Регистрация</a></li>
    </c:if>

</ul>

<div class="movie-session-container">
    <c:forEach items="${movieDTOList}" var="movies">
        <c:set var="img" value="#" />
        <c:if test="${movies.smallImage != null}">
            <c:set var="img" value="${movies.smallImage}" />
        </c:if>
        <p class="movie-title">
            <a href="${pageContext.servletContext.contextPath}/movie?id=${movies.id}">${movies.name}
                <div><img width="264" height="390"  src="${pageContext.servletContext.contextPath}${img}"></div>
            </a>
        </p>


        <c:forEach items="${localDateHashSet}" var="date">
            <c:set var="session_date_number" value="0" />
            <c:forEach items="${hallDTOList}" var="hall">
                <c:set var="cname" value="0" />
                <c:set var="session_number" value="0" />

                <c:forEach items="${movieSessionDTOList}" var="movieSession">
                    <c:if test="${movies.id == movieSession.movie.id && hall.id == movieSession.hall.id && date == movieSession.date}">
                        <c:set var="session_number" value="${session_number+1}" />
                    </c:if>
                </c:forEach>
                <c:if test="${session_number != 0}">
                    <c:if test="${session_date_number == 0}">
                        <div class="session-date">${date.format(FORMAT)}</div>
                        <c:set var="session_date_number" value="1" />
                    </c:if>



                    <c:set var="session_number" value="${100/session_number}" />
                    <c:set var="iter" value="0" />
                    <div class="showtimes-line-technology">
                        <c:forEach items="${movieSessionDTOList}" var="movieSession">

                            <c:if test="${movies.id == movieSession.movie.id && hall.id == movieSession.hall.id && date == movieSession.date}">
                                <c:if test="${cname != 1}">
                                    <div class="moviesession-hall-name">${hall.name}</div>
                                    <c:set var="cname" value="1"/>
                                </c:if>

                                <!--<div class="showtimes-line-hours">-->
                                <a href="${pageContext.servletContext.contextPath}/showChoosePlace?method=ch&session_id=${movieSession.id}"
                                   class="time  h-10   " style="left:${session_number*iter}%" title="">
                                    <c:set var="iter" value="${iter+1}" />
                                        ${movieSession.name}
                                </a>
                                <!--</div>-->
                                <!-- </div>-->

                            </c:if>
                        </c:forEach>
                    </div>
                </c:if>
            </c:forEach>
        </c:forEach>
    </c:forEach>
</div>

</body>
</html>
