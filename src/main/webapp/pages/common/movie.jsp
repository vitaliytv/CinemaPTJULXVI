<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<link href="${pageContext.servletContext.contextPath}/resources/css/create_form.css" rel="stylesheet">
<link href="${pageContext.servletContext.contextPath}/resources/css/cinema-style.css" rel="stylesheet">
<link href="${pageContext.servletContext.contextPath}/resources/css/menu.css" rel="stylesheet">


<html>
<head>
    <title>Обзор Фильма</title>
</head>

<div class="cinema-image"><img width="112" height="88" src="${pageContext.request.contextPath}/resources/image/cinema-logo.jpg"><div class="cinema_name">Кинотеатр "Black and White"</div></div>

<ul class="css-menu-3">
    <li><a href="${pageContext.servletContext.contextPath}/">Главная</a></li>
    <li><a href="${pageContext.servletContext.contextPath}/movieSession">Расписание</a></li>
    <li><a href="${pageContext.servletContext.contextPath}/moviesList">Фильмы</a></li>
    <li><a href="${pageContext.servletContext.contextPath}/cinemaDescription">Кинотеатр</a></li>
    <c:if test="${user != null}">
        <li><a href="${pageContext.servletContext.contextPath}/personalCabinet">Личные данные</a></li>
        <li><a href="${pageContext.servletContext.contextPath}/orders">Заказы</a></li>
        <li><a href="${pageContext.servletContext.contextPath}/logout">Выход</a></li>
    </c:if>
    <c:if test="${user == null}">
        <li><a href="${pageContext.servletContext.contextPath}/pages/common/login.jsp">Вход</a></li>
        <li><a href="${pageContext.servletContext.contextPath}/pages/common/registration.jsp">Регистрация</a></li>
    </c:if>

</ul>

<div><img src="${pageContext.request.contextPath}${movieDTO.image}" width="990" height="460"></div>

<div class="movie_form">
<ul>
<li>
<h2>${movieDTO.name}:</h2>
</li>
<li>
    ${movieDTO.description}
</li>
<li>
    <label>Год:</label>
    <div>${movieDTO.year}</div>

</li>
<li>
    <label>Страна:</label>
    <div>${movieDTO.country}</div>
</li>
<li>
    <label>Жанр:</label>
    <div>${movieDTO.genre}</div>
</li>
<li>
    <label>В главных ролях:</label>
    <div>${movieDTO.starring}</div>
</li>
<li>
    <label>Режисер:</label>
    <div>${movieDTO.director}</div>
</li>
<li>
    <label>Продолжительность фильма:</label>
    <div>${movieDTO.length} мин.</div>
</li>
<li>
    <label>Начало проката:</label>
    <div>${movieDTO.rentStart}</div>
</li>
<li>
    <label>Конец проката:</label>
    <div>${movieDTO.rentEnd}</div>
</li>
<li>
    <label>Ограничение по возрасту:</label>
    <div>${movieDTO.ageLimit}</div>
</li>
</ul>
</div>

</body>
</html>
