<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<link href="${pageContext.servletContext.contextPath}/resources/css/place_style.css" rel="stylesheet">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>

<html>
<head>
    <title>Выбор места</title>

</head>
<body>

<form name="input" method="post" action="${pageContext.servletContext.contextPath}/orders?method=so">
    <input type="hidden" name="movieSessionID" value="${movieSessionID}"/>
    <div class="result"></div>
    <div class="cinemaHall zal1">
        <div class="screen">Экран&nbsp;</div>
        <div class="passageBetween">&nbsp;</div>
        <c:set var="order" value="" />
        <c:set var="iterRow" value="1" />
        <c:forEach items="${placeDTOList}" var="row_place">
            <c:set var="iterPlace" value="1" />
            <c:forEach begin="1" end="${row_place.placeNumber}">
                <c:set var="placeCondition" value="0" />
                <c:if test="${ticketDTOList != null}">
                <c:forEach items="${ticketDTOList}" var="ticket">
                    <c:if test="${ticket.place.id == row_place.id && ticket.placeNumber == iterPlace}">
                        <c:set var="placeCondition" value="${ticket.ticketCondition}"/>
                    </c:if>
                </c:forEach>
                <c:if test="${placeCondition == 0}">
                    <div class="seat" data-row="${row_place.id}" data-seat="${iterPlace}"><b>${iterPlace}</b></div>
                </c:if>
                <c:if test="${placeCondition == 1}">
                    <div class="seat-sold" data-row="${iterRow}" data-seat="${iterPlace}"><b>${iterPlace}</b></div>
                </c:if>
                <c:if test="${placeCondition == 2}">
                    <div class="seat-booked" data-row="${iterRow}" data-seat="${iterPlace}"><b>${iterPlace}</b></div>
                </c:if>
                </c:if>
                <c:if test="${ticketDTOList == null}">
                    <div class="seat" data-row="${row_place.id}" data-seat="${iterPlace}"><b>${iterPlace}</b></div>
                </c:if>
                <c:set var="iterPlace" value="${iterPlace+1}" />
            </c:forEach>
            <c:set var="iterRow" value="${iterRow+1}" />
            <div class="passageBetween">&nbsp;</div>
        </c:forEach>

        <script type="text/javascript">

            $('.seat').on('click', function(e) {
                // если первый раз кликнули билет выкупили,
                // если повторно значит вернули билет
                $(e.currentTarget).toggleClass('bay');
                // $(e.currentTarget).text('trt');
                //показываем сколько билетов выкуплено
                showBaySeat();
            });

            function showBaySeat() {
                result = '';
                //ищем все места купленные и показываем список выкупленных мест
                $.each($('.seat.bay'), function(key, item) {
                    result += '<div class="ticket">Ряд: ' +
                            $(item).data().row + ' Место:' +
                            $(item).data().seat + '</div>';
                });
                $('.result').html(result);
            }

            function doOrder() {
                var order = '';
                $.each($('.seat.bay'), function(key, item) {
                    order += ';;' + $(item).data().row + ';' + $(item).data().seat;
                });
                document.getElementById('ord_id').value = order;
            }
        </script>
        <input type="hidden" name="order" id="ord_id" value=""/>
        <input onclick="doOrder()" type="submit" value="Купить" />
    </div>
</form>


</body>
</html>
