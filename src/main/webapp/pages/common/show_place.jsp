<%--
  Created by IntelliJ IDEA.
  User: Vitaliy
  Date: 23.10.2016
  Time: 15:28
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>


<link href="${pageContext.servletContext.contextPath}/resources/css/place_style.css" rel="stylesheet">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>

<html>
<head>
    <title>Расположение мест в зале ${hallDTO.name}</title>
</head>
<body>

<h3>Расположение мест в Зале: "${hallDTO.name}"</h3>
<div class="cinemaHall zal1">
    <div class="screen">Экран&nbsp;</div>
    <c:set var="iterRow" value="1" />
    <table>
    <c:forEach items="${placeDTOList}" var="row">
        <c:set var="iterPlace" value="1" />
        <tr><td><div class="row_name"><b>Ряд №${iterRow}</b></div></td>
            <td align="center">
        <c:forEach begin="1" end="${row.placeNumber}">
            <div class="seat_show" data-row="${iterRow}" data-seat="${iterPlace}"><b>${iterPlace}</b></div>
            <c:set var="iterPlace" value="${iterPlace+1}" />
        </c:forEach>
            </td></tr>
        <c:set var="iterRow" value="${iterRow+1}" />
        <div class="passageBetween">&nbsp;</div>
    </c:forEach>
    </table>
</div>

</body>
</html>
