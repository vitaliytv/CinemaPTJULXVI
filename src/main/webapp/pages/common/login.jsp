<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>

    <title>Авторизация</title>
    <link href="${pageContext.servletContext.contextPath}/resources/css/authorization.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.2.6/jquery.min.js"></script>
</head>
<body>

<div class="top">
        <span class="right">
        <a href="${pageContext.servletContext.contextPath}/">
                <strong>Вернуться назад</strong>
            </a>
        </span>
    <div class="clr"></div>
</div>

<div id="wrapper">
    <div class="user-icon"></div>
    <div class="pass-icon"></div>
    <div class="message"><c:out value="${sessionScope.message}" /></div>
    <form name="login-form" class="login-form" action="${pageContext.servletContext.contextPath}/login" method="post">

        <div class="header">
            <h1>Авторизация</h1>
            <span>Введите ваши регистрационные данные для входа. </span>
        </div>

        <div class="content">
            <input name="login" type="text" class="input username" value="Логин" onfocus="this.value=''" />
            <input name="password" type="password" class="input password" value="Пароль" onfocus="this.value=''" />
        </div>

        <div class="footer">
            <input type="submit" name="submit" value="ВОЙТИ" class="button" />
            <input type="submit" name="submit" value="Registration" class="register" />
        </div>

    </form>
</div>
<div class="gradient"></div>

<script type="text/javascript">
    $(document).ready(function() {
        $(".username").focus(function() {
            $(".user-icon").css("left","-48px");
        });
        $(".username").blur(function() {
            $(".user-icon").css("left","0px");
        });

        $(".password").focus(function() {
            $(".pass-icon").css("left","-48px");
        });
        $(".password").blur(function() {
            $(".pass-icon").css("left","0px");
        });
    });
</script>
</body>
</html>
