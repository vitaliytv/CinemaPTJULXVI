<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<link href="${pageContext.servletContext.contextPath}/resources/css/create_form.css" rel="stylesheet">


<html>
<head>
    <title>Title</title>
</head>
<body>

<ul id="navbar">
    <li><a href="${pageContext.servletContext.contextPath}/pages/admin/admin.jsp">Главная</a></li>
    <li><a>Списки</a>
        <ul>
            <li><a href="${pageContext.servletContext.contextPath}/admin/list/HallList">Залы</a></li>
            <li><a href="${pageContext.servletContext.contextPath}/admin/list/MovieList">Фильмы</a></li>
            <li><a href="${pageContext.servletContext.contextPath}/admin/list/SessionList">Сеансы</a></li>
            <li><a class="selected">Билеты</a></li>
            <li><a href="${pageContext.servletContext.contextPath}/admin/list/UserList">Пользователи</a></li>
        </ul>
    </li>
    <li><a>Добавить</a>
        <ul>
            <li><a href="${pageContext.servletContext.contextPath}/pages/admin/create/hall_create.jsp">Зал</a></li>
            <li><a href="${pageContext.servletContext.contextPath}/pages/admin/create/movie_create.jsp">Фильм</a></li>
            <li><a href="${pageContext.servletContext.contextPath}/admin/createSession">Сеанс</a></li>
        </ul>
    </li>
    <li><a href="${pageContext.servletContext.contextPath}/logout">Logout</a></li>
</ul>


<div class="movie_form">
    <ul>
        <li>
            <h2>Список билетов:</h2>
        </li>
        <c:set var="date" value=""/>
        <c:forEach items="${ticketDTOList}" var="ticket">
        <li>
            <c:if test="${date != ticket.date}">
                <h3>${sdf.format(ticket.date)}</h3>
                <c:set var="date" value="${ticket.date}"/>
            </c:if>
            <div>Имя, фамилия: ${ticket.user.name} ${ticket.user.surname}</div>
            <div>Сеанс:  ${ticket.movieSession.date.format(FORMAT)}  ${ticket.movieSession.name}</div>
            <div>Фильм:  ${ticket.movieSession.movie.name}</div>
            <div>Зал:  ${ticket.movieSession.hall.name}</div>
            <div>Ряд: ${ticket.place.rowNumber}</div>
            <div>Место  ${ticket.placeNumber}</div>
            <div>Цена:  ${ticket.price}</div>
        </li>
        </c:forEach>
    </ul>
</div>


</body>
</html>
