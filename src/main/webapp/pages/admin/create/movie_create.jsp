<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<link href="${pageContext.servletContext.contextPath}/resources/css/create_form.css" rel="stylesheet">
<link href="${pageContext.servletContext.contextPath}/resources/css/cinema-style.css" rel="stylesheet">

<html>
<head>
    <title>Добавление Фильма</title>
</head>
<body>

<ul id="navbar">
    <li><a href="${pageContext.servletContext.contextPath}/pages/admin/admin.jsp">Главная</a></li>
    <li><a>Списки</a>
        <ul>
            <li><a href="${pageContext.servletContext.contextPath}/admin/list/HallList">Залы</a></li>
            <li><a href="${pageContext.servletContext.contextPath}/admin/list/MovieList">Фильмы</a></li>
            <li><a href="${pageContext.servletContext.contextPath}/admin/list/SessionList">Сеансы</a></li>
            <li><a href="${pageContext.servletContext.contextPath}/admin/list/TicketList">Билеты</a></li>
            <li><a href="${pageContext.servletContext.contextPath}/admin/list/UserList">Пользователи</a></li>
        </ul>
    </li>
    <li><a>Добавить</a>
        <ul>
            <li><a href="${pageContext.servletContext.contextPath}/pages/admin/create/hall_create.jsp">Зал</a></li>
            <li><a href="${pageContext.servletContext.contextPath}/pages/admin/create/movie_create.jsp" class="selected">Фильм</a></li>
            <li><a href="${pageContext.servletContext.contextPath}/admin/createSession">Сеанс</a></li>
        </ul>
    </li>
    <li><a href="${pageContext.servletContext.contextPath}/logout">Logout</a></li>
</ul>


<form class="movie_form" name="movie_form" method="post" action="${pageContext.servletContext.contextPath}/admin/createMovie">
    <ul>
        <li>
            <h3>Данные по фильму:</h3>
        </li>
        <li>
            <label for="name">Название:</label>
            <input type="text" name="name" required/> <br/>

        </li>
        <li>
            <label for="year">Год:</label>
            <input type="number" name="year" required/> <br/>

        </li>
        <li>
            <label for="country">Страна:</label>
            <input type="text" name="country" required/> <br/>
        </li>
        <li>
            <label for="genre">Жанр:</label>
            <input type="text" name="genre" required/> <br/>
        </li>
        <li>
            <label for="starring">В главных ролях:</label>
            <textarea name="starring" cols="50" rows="3" required></textarea>
        </li>
        <li>
            <label for="director">Режисер:</label>
            <input type="text" name="director" required/> <br/>
        </li>
        <li>
            <label for="length">Продолжительность фильма:</label>
            <input type="number" name="length" required/> <br/>
        </li>
        <li>
            <label for="rent_start">Начало проката:</label>
            <input type="date" name="rent_start" required/> <br/>
        </li>
        <li>
            <label for="rent_end">Конец проката:</label>
            <input type="date" name="rent_end" required/> <br/>
        </li>
        <li>
            <label for="description">Описание:</label>
            <textarea name="description" cols="100" rows="10" required></textarea>
        </li>
        <li>
            <label for="age_limit">Ограничение по возрасту:</label>
            <input type="text" name="age_limit" required/> <br/>
        </li>
        <li>
            <label for="image">Путь к постеру(картинке) фильма:</label>
            <input type="text" name="image" required/> <br/>
        </li>
        <li>
            <label for="small_image">Путь к маленькой картинке фильма:</label>
            <input type="text" name="small_image" required/> <br/>
        </li>
        <li>
            <button class="submit" type="submit">Сохранить Фильм</button>
        </li>
    </ul>
</form>


</body>
</html>
