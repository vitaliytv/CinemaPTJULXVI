<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<link href="${pageContext.servletContext.contextPath}/resources/css/create_form.css" rel="stylesheet">
<link href="${pageContext.servletContext.contextPath}/resources/css/cinema-style.css" rel="stylesheet">

<html>
<head>
    <title>Добавление Сеанса</title>
</head>
<body>

<ul id="navbar">
    <li><a href="${pageContext.servletContext.contextPath}/pages/admin/admin.jsp">Главная</a></li>
    <li><a>Списки</a>
        <ul>
            <li><a href="${pageContext.servletContext.contextPath}/admin/list/HallList">Залы</a></li>
            <li><a href="${pageContext.servletContext.contextPath}/admin/list/MovieList">Фильмы</a></li>
            <li><a href="${pageContext.servletContext.contextPath}/admin/list/SessionList">Сеансы</a></li>
            <li><a href="${pageContext.servletContext.contextPath}/admin/list/TicketList">Билеты</a></li>
            <li><a href="${pageContext.servletContext.contextPath}/admin/list/UserList">Пользователи</a></li>
        </ul>
    </li>
    <li><a>Добавить</a>
        <ul>
            <li><a href="${pageContext.servletContext.contextPath}/pages/admin/create/hall_create.jsp">Зал</a></li>
            <li><a href="${pageContext.servletContext.contextPath}/pages/admin/create/movie_create.jsp">Фильм</a></li>
            <li><a class="selected">Сеанс</a></li>
        </ul>
    </li>
    <li><a href="${pageContext.servletContext.contextPath}/logout">Logout</a></li>
</ul>

<form class="movie_form" name="movie_session_form" method="post" action="${pageContext.servletContext.contextPath}/admin/saveSession">
    <ul>
        <li>
            <h3>Введите информацию о сеансе:</h3>
        </li>
        <li>
            <label for="name">Название Сеанса:</label>
            <input type="text" name="name" required/> <br/>
        </li>
        <li>
            <label for="date">Дату сеанса:</label>
            <input type="datetime-local" name="date" required/> <br/>
        </li>
        <li>
            <label for="movie[]">Выберите Фильм:</label>
            <p><select size="4" name="movie[]">
                <c:forEach items="${movieDTOList}" var="movies">
                    <option value="${movies.id}">${movies.name}</option>
                </c:forEach>
            </select></p>
        </li>
        <li>
            <label for="hall[]">Выберите Зал:</label>
            <p><select size="4" name="hall[]">
                <c:forEach items="${hallDTOList}" var="hall">
                    <option value="${hall.id}">${hall.name}</option>
                </c:forEach>
            </select></p>
        </li>
        <li>
            <button class="submit" type="submit">Сохранить Сеанс</button>
        </li>
    </ul>

</form>
</body>
</html>
