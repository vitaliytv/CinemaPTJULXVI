<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<link href="${pageContext.servletContext.contextPath}/resources/css/create_form.css" rel="stylesheet">
<link href="${pageContext.servletContext.contextPath}/resources/css/cinema-style.css" rel="stylesheet">

<html>
<head>
    <title>Добавление Зала</title>
</head>
<body>

<ul id="navbar">
    <li><a href="${pageContext.servletContext.contextPath}/pages/admin/admin.jsp">Главная</a></li>
    <li><a>Списки</a>
        <ul>
            <li><a href="${pageContext.servletContext.contextPath}/admin/list/HallList">Залы</a></li>
            <li><a href="${pageContext.servletContext.contextPath}/admin/list/MovieList">Фильмы</a></li>
            <li><a href="${pageContext.servletContext.contextPath}/admin/list/SessionList">Сеансы</a></li>
            <li><a href="${pageContext.servletContext.contextPath}/admin/list/TicketList">Билеты</a></li>
            <li><a href="${pageContext.servletContext.contextPath}/admin/list/UserList">Пользователи</a></li>
        </ul>
    </li>
    <li><a>Добавить</a>
        <ul>
            <li><a class="selected">Зал</a></li>
            <li><a href="${pageContext.servletContext.contextPath}/pages/admin/create/movie_create.jsp">Фильм</a></li>
            <li><a href="${pageContext.servletContext.contextPath}/admin/createSession">Сеанс</a></li>
        </ul>
    </li>
    <li><a href="${pageContext.servletContext.contextPath}/logout">Logout</a></li>
</ul>

<script type="text/javascript">
    var countOfFields = 1;
    var curFieldNameId = 1;
    var maxFieldLimit = 100;

    function deleteField(a) {
        var contDiv = a.parentNode.parentNode;
        contDiv.parentNode.removeChild(contDiv);
        curFieldNameId--;
        countOfFields--;
        return false;
    }

    function addField() {
        if (countOfFields >= maxFieldLimit) {
            alert("Число полей достигло своего максимума = " + maxFieldLimit);
            return false;
        }
        countOfFields++;
        curFieldNameId++;
        var div = document.createElement("div");
        div.innerHTML = "<p>Ряд №"+curFieldNameId+":<input  class=\"input_number\" type=\"number\" name=\"row_"+curFieldNameId+"\"  id=\"row"+curFieldNameId+"\"  />"+"<button class=\"select\" onclick=\"return  deleteField(this)\" type=\"button\">X</button></p>";
        document.getElementById("parentId").appendChild(div);
        return false;
    }
</script>

<form class="movie_form" name="hall_form" method="post" action="${pageContext.servletContext.contextPath}/admin/createHall">
    <ul>
        <li>
            <h3>Введите информацию о зале:</h3>
        </li>
        <li>
            <label for="name">Название Зала:</label>
            <input type="text" name="name"/> <br/>
        </li>
        <li>
            <label for="description">Описание:</label>
            <input type="text" name="description"/> <br/>
        </li>
        <li>
            <label for="select">Введите колличество мест в рядах:</label>
            <button class="select" onclick="return addField()" type="button">Добавить ряд</button>
        </li>
        <li>
            <div id="parentId">
                <p>Ряд №1:<input class="input_number" type="number" name="row_1" id="1" value=""  /></p>
            </div>
        </li>


        <li>
            <button class="submit" type="submit">Сохранить Зал</button>
        </li>
    </ul>

</form>

</body>
</html>
