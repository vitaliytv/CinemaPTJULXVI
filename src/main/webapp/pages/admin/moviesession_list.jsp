<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<link href="${pageContext.servletContext.contextPath}/resources/css/menu_style.css" rel="stylesheet">
<link href="${pageContext.servletContext.contextPath}/resources/css/cinema-style.css" rel="stylesheet">
<html>
<head>
    <title>Список Сеансов</title>
</head>
<body>

<ul id="navbar">
    <li><a href="${pageContext.servletContext.contextPath}/pages/admin/admin.jsp">Главная</a></li>
    <li><a>Списки</a>
        <ul>
            <li><a href="${pageContext.servletContext.contextPath}/admin/list/HallList">Залы</a></li>
            <li><a href="${pageContext.servletContext.contextPath}/admin/list/MovieList">Фильмы</a></li>
            <li><a class="selected">Сеансы</a></li>
            <li><a href="${pageContext.servletContext.contextPath}/admin/list/TicketList">Билеты</a></li>
            <li><a href="${pageContext.servletContext.contextPath}/admin/list/UserList">Пользователи</a></li>
        </ul>
    </li>
    <li><a>Добавить</a>
        <ul>
            <li><a href="${pageContext.servletContext.contextPath}/pages/admin/create/hall_create.jsp">Зал</a></li>
            <li><a href="${pageContext.servletContext.contextPath}/pages/admin/create/movie_create.jsp">Фильм</a></li>
            <li><a href="${pageContext.servletContext.contextPath}/admin/createSession">Сеанс</a></li>
        </ul>
    </li>
    <li><a href="${pageContext.servletContext.contextPath}/logout">Logout</a></li>
</ul>

<div class="session-movie-container-body">
    <c:forEach items="${movieDTOList}" var="movies">
            <p><img width="132" height="195"  src="${pageContext.servletContext.contextPath}${movies.smallImage}"> ${movies.name}</p>
        <c:forEach items="${localDateHashSet}" var="date">
            <c:set var="session_date_number" value="0" />
            <c:forEach items="${hallDTOList}" var="hall">
                <c:set var="cname" value="0" />
                <c:set var="session_number" value="0" />
                <c:forEach items="${movieSessionDTOList}" var="movieSession">
                    <c:if test="${movies.id == movieSession.movie.id && hall.id == movieSession.hall.id && date == movieSession.date}">
                        <c:set var="session_number" value="${session_number+1}" />
                    </c:if>
                </c:forEach>
                <c:if test="${session_number != 0}">
                    <c:if test="${session_date_number == 0}">
                        <div class="session-date">${date.format(FORMAT)}</div>
                        <c:set var="session_date_number" value="1" />
                    </c:if>

                <c:set var="session_number" value="${60/session_number}" />
                <c:set var="iter" value="0" />
                <div class="showtimes-line-technology">
                    <c:forEach items="${movieSessionDTOList}" var="movieSession">

                        <c:if test="${movies.id == movieSession.movie.id && hall.id == movieSession.hall.id && date == movieSession.date}">
                            <c:if test="${cname != 1}">
                                <div class="moviesession-hall-name">${hall.name}</div>
                                <c:set var="cname" value="1"/>
                            </c:if>


                            <a href="${pageContext.servletContext.contextPath}/pages/user/cabinet/hall?session_id=${movieSession.id}"
                               class="time  h-10" style="left: ${session_number*iter}%" title="">
                                <c:set var="iter" value="${iter+1}" />
                                    ${movieSession.name}
                                <a href="${pageContext.servletContext.contextPath}/admin/updateMovieSession?id=${movieSession.id}&method=sms"
                                   class="options" style="left: ${session_number*iter}%">Edit</a>
                                <a href="${pageContext.servletContext.contextPath}/admin/updateMovieSession?id=${movieSession.id}&method=dms"
                                   class="options" style="left: ${(session_number+1)*iter}%">Delete</a>
                            </a>


                        </c:if>
                    </c:forEach>
                </div>
                </c:if>
            </c:forEach>
        </c:forEach>
    </c:forEach>
</div>

</body>
</html>
