<%--
  Created by IntelliJ IDEA.
  User: Vitaliy
  Date: 29.10.2016
  Time: 12:17
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<link href="${pageContext.servletContext.contextPath}/resources/css/menu_style.css" rel="stylesheet">

<html>
<head>
    <title>Изменение данных о Фильме</title>
</head>
<body>

<ul id="navbar">
    <li><a href="${pageContext.servletContext.contextPath}/pages/admin/admin.jsp">Главная</a></li>
    <li><a>Списки</a>
        <ul>
            <li><a href="${pageContext.servletContext.contextPath}/admin/list/HallList">Залы</a></li>
            <li><a href="${pageContext.servletContext.contextPath}/admin/list/MovieList">Фильмы</a></li>
            <li><a href="${pageContext.servletContext.contextPath}/admin/list/SessionList">Сеансы</a></li>
            <li><a href="${pageContext.servletContext.contextPath}/admin/list/TicketList">Билеты</a></li>
            <li><a href="${pageContext.servletContext.contextPath}/admin/list/UserList">Пользователи</a></li>
        </ul>
    </li>
    <li><a>Добавить</a>
        <ul>
            <li><a href="${pageContext.servletContext.contextPath}/pages/admin/create/hall_create.jsp">Зал</a></li>
            <li><a href="${pageContext.servletContext.contextPath}/pages/admin/create/movie_create.jsp">Фильм</a></li>
            <li><a href="${pageContext.servletContext.contextPath}/admin/createSession">Сеанс</a></li>
        </ul>
    </li>
    <li><a href="${pageContext.servletContext.contextPath}/logout">Logout</a></li>
</ul>

<form name="movieForm" method="post" action="${pageContext.servletContext.contextPath}/admin/updateMovie?method=sv">
    <h3>Изменение данных по фильму:</h3>
    <input type="hidden" name="id" value="${movieDTO.id}" />
    Название: <input type="text" name="name" value="${movieDTO.name}" /> <br/>
    Год: <input type="text" name="year" value="${movieDTO.year}" /> <br/>
    Страна: <input type="text" name="country" value="${movieDTO.country}" /> <br/>
    Жанр: <input type="text" name="genre" value="${movieDTO.genre}" /> <br/>
    В главных ролях: <input type="text" name="starring" value="${movieDTO.starring}" /> <br/>
    Режисер: <input type="text" name="director" value="${movieDTO.director}" /> <br/>
    Продолжительность фильма: <input type="text" name="length" value="${movieDTO.length}" /> <br/>
    Начало проката: <input type="date" name="rent_start" value="${movieDTO.rentStart}" /> <br/>
    Конец проката: <input type="date" name="rent_end" value="${movieDTO.rentEnd}" /> <br/>
    Описание: <input type="text" name="description" value="${movieDTO.description}" /> <br/>
    Ограничение по возрасту: <input type="text" name="age_limit" value="${movieDTO.ageLimit}" /> <br/>
    Путь к постеру(картинке) фильма: <input type="text" name="image" value="${movieDTO.image}" /> <br/>
    Путь к маленькой картинке фильма: <input type="text" name="small_image" value="${movieDTO.smallImage}" /> <br/>
    <input type="submit" value="UpdateMovie" />
</form>

</body>
</html>
