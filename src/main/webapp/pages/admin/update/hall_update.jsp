<%--
  Created by IntelliJ IDEA.
  User: Vitaliy
  Date: 29.10.2016
  Time: 12:17
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<link href="${pageContext.servletContext.contextPath}/resources/css/menu_style.css" rel="stylesheet">

<html>
<head>
    <title>Обновление информации о зале</title>
</head>
<body>

<ul id="navbar">
    <li><a href="${pageContext.servletContext.contextPath}/pages/admin/admin.jsp">Главная</a></li>
    <li><a>Списки</a>
        <ul>
            <li><a href="${pageContext.servletContext.contextPath}/admin/list/HallList">Залы</a></li>
            <li><a href="${pageContext.servletContext.contextPath}/admin/list/MovieList">Фильмы</a></li>
            <li><a href="${pageContext.servletContext.contextPath}/admin/list/SessionList">Сеансы</a></li>
            <li><a href="${pageContext.servletContext.contextPath}/admin/list/TicketList">Билеты</a></li>
            <li><a href="${pageContext.servletContext.contextPath}/admin/list/UserList">Пользователи</a></li>
        </ul>
    </li>
    <li><a>Добавить</a>
        <ul>
            <li><a href="${pageContext.servletContext.contextPath}/pages/admin/create/hall_create.jsp">Зал</a></li>
            <li><a href="${pageContext.servletContext.contextPath}/pages/admin/create/movie_create.jsp">Фильм</a></li>
            <li><a href="${pageContext.servletContext.contextPath}/admin/createSession">Сеанс</a></li>
        </ul>
    </li>
    <li><a href="${pageContext.servletContext.contextPath}/logout">Logout</a></li>
</ul>

<script type="text/javascript">
    var countOfFields = 0;
    var curFieldNameId = 0;
    var maxFieldLimit = 100;

    function deleteField(a) {
        var contDiv = a.parentNode.parentNode;
        contDiv.parentNode.removeChild(contDiv);
        curFieldNameId--;
        countOfFields--;
        return false;
    }

    function addField() {
        if (countOfFields >= maxFieldLimit) {
            alert("Число полей достигло своего максимума = " + maxFieldLimit);
            return false;
        }
        countOfFields++;
        curFieldNameId++;
        var div = document.createElement("div");
        div.innerHTML = "<p>Ряд №"+curFieldNameId+":<input  type=\"text\" name=\"row_"+
                curFieldNameId+"\"  id=\"row"+curFieldNameId+
                "\" class=\"textfield\" style=\"width:  20px;\" />"
                +"<input onclick=\"return  deleteField(this)\" type=\"button\" value=\"Х\" /></p>";
        document.getElementById("parentId").appendChild(div);
        return false;
    }

    function createField(rowNumber,placeNumber) {
        if (countOfFields >= maxFieldLimit) {
            alert("Число полей достигло своего максимума = " + maxFieldLimit);
            return false;
        }
        countOfFields++;
        curFieldNameId++;
        var div = document.createElement("div");

        if(countOfFields == 1){
            div.innerHTML = "<p>Ряд №"+rowNumber+":<input  type=\"text\" name=\"row_"+
                    rowNumber+"\"  id=\"row"+rowNumber+"\" value=\""+placeNumber+"\" class=\"textfield\" " +
                    "style=\"width:  20px;\" /></p>";
        }else{
            div.innerHTML = "<p>Ряд №"+rowNumber+":<input  type=\"text\" name=\"row_"+
                    rowNumber+"\"  id=\"row"+rowNumber+"\" value=\""+placeNumber+"\" class=\"textfield\" " +
                    "style=\"width:  20px;\" /><input onclick=\"return  deleteField(this)\" type=\"button\" value=\"Х\" /></p>";
        }

        document.getElementById("parentId").appendChild(div);
        return false;
    }
</script>


<form name="hallUpdateForm" method="post" action="${pageContext.servletContext.contextPath}/admin/updateHall?method=sv">
    <h3>Введите информацию о зале:</h3>
    <input type="hidden" name="id" value="${hallDTO.id}" />
    Название Зала: <input type="text" name="name" value="${hallDTO.name}"/> <br/>
    Описание: <input type="text" name="description" value="${hallDTO.description}"/> <br/>
    <input onclick="return addField()" type="button" value="Добавить ряд" />
    <div id="parentId"></div>

    <c:forEach items="${placeDTOList}" var="place">
        <script type="text/javascript">
            createField(${place.rowNumber},${place.placeNumber});
        </script>
    </c:forEach>

    <input type="submit" value="SaveHall" />
</form>
</body>
</html>
