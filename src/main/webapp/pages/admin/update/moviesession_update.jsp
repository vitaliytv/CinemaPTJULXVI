<%--
  Created by IntelliJ IDEA.
  User: Vitaliy
  Date: 29.10.2016
  Time: 12:17
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<link href="${pageContext.servletContext.contextPath}/resources/css/menu_style.css" rel="stylesheet">

<html>
<head>
    <title>Изменение данных о Сеансе</title>
</head>
<body>

<ul id="navbar">
    <li><a href="${pageContext.servletContext.contextPath}/pages/admin/admin.jsp">Главная</a></li>
    <li><a>Списки</a>
        <ul>
            <li><a href="${pageContext.servletContext.contextPath}/admin/list/HallList">Залы</a></li>
            <li><a href="${pageContext.servletContext.contextPath}/admin/list/MovieList">Фильмы</a></li>
            <li><a href="${pageContext.servletContext.contextPath}/admin/list/SessionList">Сеансы</a></li>
            <li><a href="${pageContext.servletContext.contextPath}/admin/list/TicketList">Билеты</a></li>
            <li><a href="${pageContext.servletContext.contextPath}/admin/list/UserList">Пользователи</a></li>
        </ul>
    </li>
    <li><a>Добавить</a>
        <ul>
            <li><a href="${pageContext.servletContext.contextPath}/pages/admin/create/hall_create.jsp">Зал</a></li>
            <li><a href="${pageContext.servletContext.contextPath}/pages/admin/create/movie_create.jsp">Фильм</a></li>
            <li><a href="${pageContext.servletContext.contextPath}/admin/createSession">Сеанс</a></li>
        </ul>
    </li>
    <li><a href="${pageContext.servletContext.contextPath}/logout">Logout</a></li>
</ul>

<form name="movieSessionCreateForm" method="post" action="${pageContext.servletContext.contextPath}/admin/updateMovieSession?method=sv">
    <h3>Введите информацию о сеансе:</h3>
    <input type="hidden" name="id" value="${movieSessionDTO.id}"/>
    Название Сеанса: <input type="text" name="name" value="${movieSessionDTO.name}"/> <br/>
    Дату сеанса: <input type="datetime-local" name="date" value="${movieSessionDTO.date}"/> <br/>

    <p><select size="3" name="movie[]">
        <option disabled>Выберите Фильм</option>
        <c:forEach items="${movieDTOList}" var="movies">
            <c:if test="${movieSessionDTO.movie.id == movies.id}">
                <option selected value="${movies.id}">${movies.name}</option>
            </c:if>
            <c:if test="${movieSessionDTO.movie.id != movies.id}">
                <option value="${movies.id}">${movies.name}</option>
            </c:if>
        </c:forEach>
    </select></p>

    <p><select size="3" name="hall[]">
        <option disabled>Выберите Зал</option>
        <c:forEach items="${hallDTOList}" var="hall">
            <c:if test="${movieSessionDTO.hall.id == hall.id}">
                <option selected value="${hall.id}">${hall.name}</option>
            </c:if>
            <c:if test="${movieSessionDTO.hall.id != hall.id}">
                <option value="${hall.id}">${hall.name}</option>
            </c:if>
        </c:forEach>
    </select></p>

    <input type="submit" value="SaveMovieSession" />
</form>

</body>
</html>
