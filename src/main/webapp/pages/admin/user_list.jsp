<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<link href="${pageContext.servletContext.contextPath}/resources/css/menu_style.css" rel="stylesheet">
<link href="${pageContext.servletContext.contextPath}/resources/css/table.css" rel="stylesheet">

<html>
<head>
    <title>Title</title>
</head>
<body>

<ul id="navbar">
    <li><a href="${pageContext.servletContext.contextPath}/pages/admin/admin.jsp">Главная</a></li>
    <li><a>Списки</a>
        <ul>
            <li><a href="${pageContext.servletContext.contextPath}/admin/list/HallList">Залы</a></li>
            <li><a href="${pageContext.servletContext.contextPath}/admin/list/MovieList">Фильмы</a></li>
            <li><a href="${pageContext.servletContext.contextPath}/admin/list/SessionList">Сеансы</a></li>
            <li><a href="${pageContext.servletContext.contextPath}/admin/list/TicketList">Билеты</a></li>
            <li><a class="selected">Пользователи</a></li>
        </ul>
    </li>
    <li><a>Добавить</a>
        <ul>
            <li><a href="${pageContext.servletContext.contextPath}/pages/admin/create/hall_create.jsp">Зал</a></li>
            <li><a href="${pageContext.servletContext.contextPath}/pages/admin/create/movie_create.jsp">Фильм</a></li>
            <li><a href="${pageContext.servletContext.contextPath}/admin/createSession">Сеанс</a></li>
        </ul>
    </li>
    <li><a href="${pageContext.servletContext.contextPath}/logout">Logout</a></li>
</ul>

<table class="table_show" border="2">
    <tr>
        <th>
            ID Пользователя
        </th>
        <th>
            Имя, Фамилия
        </th>
        <th>
            E-mail
        </th>
        <th>
            Login
        </th>
        <th>
            Телефон
        </th>
        <th>
            Роль
        </th>
        <th>
            Опции
        </th>

    </tr>
    <c:forEach items="${userDTOList}" var="users">
        <tr>
            <td>
                    ${users.id}
            </td>
            <td>
                    ${users.name} ${users.surname}
            </td>
            <td>
                    ${users.email}
            </td>
            <td>
                    ${users.login}
            </td>
            <td>
                    ${users.phone}
            </td>
            <td>
                    ${users.role.name}
            </td>
            <td>
                <a href="${pageContext.servletContext.contextPath}/admin/updateUser?id=${users.id}">Delete</a>
            </td>
        </tr>
    </c:forEach>
</table>

</body>
</html>
