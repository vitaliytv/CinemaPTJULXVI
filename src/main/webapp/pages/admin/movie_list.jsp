<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<link href="${pageContext.servletContext.contextPath}/resources/css/menu_style.css" rel="stylesheet">
<link href="${pageContext.servletContext.contextPath}/resources/css/table.css" rel="stylesheet">

<html>
<head>
    <title>Список Фильмов</title>
</head>
<body>

<ul id="navbar">
    <li><a href="${pageContext.servletContext.contextPath}/pages/admin/admin.jsp">Главная</a></li>
    <li><a>Списки</a>
        <ul>
            <li><a href="${pageContext.servletContext.contextPath}/admin/list/HallList">Залы</a></li>
            <li><a class="selected">Фильмы</a></li>
            <li><a href="${pageContext.servletContext.contextPath}/admin/list/SessionList">Сеансы</a></li>
            <li><a href="${pageContext.servletContext.contextPath}/admin/list/TicketList">Билеты</a></li>
            <li><a href="${pageContext.servletContext.contextPath}/admin/list/UserList">Пользователи</a></li>
        </ul>
    </li>
    <li><a>Добавить</a>
        <ul>
            <li><a href="${pageContext.servletContext.contextPath}/pages/admin/create/hall_create.jsp">Зал</a></li>
            <li><a href="${pageContext.servletContext.contextPath}/pages/admin/create/movie_create.jsp">Фильм</a></li>
            <li><a href="${pageContext.servletContext.contextPath}/admin/createSession">Сеанс</a></li>
        </ul>
    </li>
    <li><a href="${pageContext.servletContext.contextPath}/logout">Logout</a></li>
</ul>

<table class="table_show" border="2">
    <tr>
        <th>
            ID Фильма
        </th>
        <th>
            Название
        </th>
        <th>
            Описание
        </th>
        <th>
            Small постер
        </th>
        <th>
            В главных ролях
        </th>
        <th>
            Опции
        </th>

    </tr>
    <c:forEach items="${movieDTOList}" var="movies">
    <tr>

        <td>
            ${movies.id}
        </td>
        <td>
            ${movies.name}
        </td>
        <td>
            ${movies.description}
        </td>
        <td>
            <b>путь:</b> ${movies.smallImage}
            <img src="${pageContext.request.contextPath}${movies.smallImage}" width="160" height="236">

        </td>
        <td>
                ${movies.starring}
        </td>
        <td>
            <a href="${pageContext.servletContext.contextPath}/admin/updateMovie?id=${movies.id}&method=sm">Edit</a>
            <a href="${pageContext.servletContext.contextPath}/admin/updateMovie?id=${movies.id}&method=dm">Delete</a>
        </td>
    </tr>
    </c:forEach>
</table>

</body>
</html>
