<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<link href="${pageContext.servletContext.contextPath}/resources/css/menu_style.css" rel="stylesheet">
<link href="${pageContext.servletContext.contextPath}/resources/css/table.css" rel="stylesheet">

<html>
<head>
    <title>Список Залов</title>
</head>
<body>
<ul id="navbar">
    <li><a href="${pageContext.servletContext.contextPath}/pages/admin/admin.jsp">Главная</a></li>
    <li><a>Списки</a>
        <ul>
            <li><a class="selected">Залы</a></li>
            <li><a href="${pageContext.servletContext.contextPath}/admin/list/MovieList">Фильмы</a></li>
            <li><a href="${pageContext.servletContext.contextPath}/admin/list/SessionList">Сеансы</a></li>
            <li><a href="${pageContext.servletContext.contextPath}/admin/list/TicketList">Билеты</a></li>
            <li><a href="${pageContext.servletContext.contextPath}/admin/list/UserList">Пользователи</a></li>
        </ul>
    </li>
    <li><a>Добавить</a>
        <ul>
            <li><a href="${pageContext.servletContext.contextPath}/pages/admin/create/hall_create.jsp">Зал</a></li>
            <li><a href="${pageContext.servletContext.contextPath}/pages/admin/create/movie_create.jsp">Фильм</a></li>
            <li><a href="${pageContext.servletContext.contextPath}/admin/createSession">Сеанс</a></li>
        </ul>
    </li>
    <li><a href="${pageContext.servletContext.contextPath}/logout">Logout</a></li>
</ul>

<table class="table_show" border="2">
    <tr>
        <th>
            ID Зала
        </th>
        <th>
            Название
        </th>
        <th>
            Описание
        </th>
        <th>
            Ряд, кол-во мест
        </th>
        <th>
            Опции
        </th>

    </tr>
    <c:forEach items="${hallDTOList}" var="hall">
        <tr>

            <td>
                    ${hall.id}
            </td>
            <td>
                    ${hall.name}
            </td>
            <td>
                    ${hall.description}
            </td>
            <td>
            <c:forEach items="${placeDTOList}" var="place">
                <c:if test="${hall.id == place.hall.id}">
                           <p>${place.rowNumber}, ${place.placeNumber}</p>
                </c:if>
            </c:forEach>
            </td>
            <td>
                <a href="${pageContext.servletContext.contextPath}/admin/updateHall?id=${hall.id}&method=sh">Edit</a>
                <a href="${pageContext.servletContext.contextPath}/admin/updateHall?id=${hall.id}&method=dh">Delete</a>
            </td>
        </tr>
    </c:forEach>
</table>

</body>
</html>
