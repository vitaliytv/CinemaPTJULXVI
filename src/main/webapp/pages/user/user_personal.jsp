<%--
  Created by IntelliJ IDEA.
  User: Vitaliy
  Date: 31.10.2016
  Time: 13:36
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<link href="${pageContext.servletContext.contextPath}/resources/css/menu.css" rel="stylesheet">
<link href="${pageContext.servletContext.contextPath}/resources/css/cinema-style.css" rel="stylesheet">
<link href="${pageContext.servletContext.contextPath}/resources/css/create_form.css" rel="stylesheet">


<html>
<head>
    <title>Title</title>
</head>
<body>

<div class="cinema-image"><img width="112" height="88" src="${pageContext.request.contextPath}/resources/image/cinema-logo.jpg"><div class="cinema_name">Кинотеатр "Black and White"</div></div>

<ul class="css-menu-3">
    <li><a href="${pageContext.servletContext.contextPath}/">Главная</a></li>
    <li><a href="${pageContext.servletContext.contextPath}/movieSession">Расписание</a></li>
    <li><a href="${pageContext.servletContext.contextPath}/moviesList">Фильмы</a></li>
    <li><a href="${pageContext.servletContext.contextPath}/cinemaDescription">Кинотеатр</a></li>
    <li><a href="${pageContext.servletContext.contextPath}/personalCabinet">Личные данные</a></li>
    <li><a href="${pageContext.servletContext.contextPath}/orders">Заказы</a></li>
    <li><a href="${pageContext.servletContext.contextPath}/logout">Выход</a></li>

</ul>

<div class="movie_form">
    <ul>
        <li>
            <h2>Личные данные:</h2>
        </li>
        <li>
            <label>Имя:</label>
            <div>${userDTO.name}</div>

        </li>
        <li>
            <label>Фамилия:</label>
            <div>${userDTO.surname}</div>
        </li>
        <li>
            <label>e-mail:</label>
            <div>${userDTO.email}</div>
        </li>
        <li>
            <label>login:</label>
            <div>${userDTO.login}</div>
        </li>
        <li>
            <label>Телефон:</label>
            <div>${userDTO.phone}</div>
        </li>
        <li>
            <label>День рождения:</label>
            <div>${userDTO.birthday}</div>
        </li>
        <li>
            <label>Пол:</label>
            <div>${userDTO.sex}</div>
        </li>
    </ul>
</div>


</body>
</html>
