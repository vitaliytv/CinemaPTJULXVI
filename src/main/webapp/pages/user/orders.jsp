<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<link href="${pageContext.servletContext.contextPath}/resources/css/menu.css" rel="stylesheet">
<link href="${pageContext.servletContext.contextPath}/resources/css/cinema-style.css" rel="stylesheet">
<link href="${pageContext.servletContext.contextPath}/resources/css/create_form.css" rel="stylesheet">


<html>
<head>
    <title>Заказы</title>
</head>
<body>

<div class="cinema-image"><img width="112" height="88" src="${pageContext.request.contextPath}/resources/image/cinema-logo.jpg"><div class="cinema_name">Кинотеатр "Black and White"</div></div>

<ul class="css-menu-3">
    <li><a href="${pageContext.servletContext.contextPath}/">Главная</a></li>
    <li><a href="${pageContext.servletContext.contextPath}/movieSession">Расписание</a></li>
    <li><a href="${pageContext.servletContext.contextPath}/moviesList">Фильмы</a></li>
    <li><a href="${pageContext.servletContext.contextPath}/cinemaDescription">Кинотеатр</a></li>
    <li><a href="${pageContext.servletContext.contextPath}/personalCabinet">Личные данные</a></li>
    <li><a href="${pageContext.servletContext.contextPath}/orders">Заказы</a></li>
    <li><a href="${pageContext.servletContext.contextPath}/logout">Выход</a></li>

</ul>
<c:if test="${ticketDTOList != null}">
    <div class="movie_form">
        <ul>
            <li>
                <h2>Билеты:</h2>
            </li>
            <c:set var="dat" value=""/>
            <c:forEach items="${ticketDTOList}" var="ticket">
                <c:if test="${dat != ticket.date}">
                <li>
                    <label>Дата продажи:</label>
                    <div>${sdf.format(ticket.date)}</div>

                </li>
                <c:set var="dat" value="${ticket.date}"/>
                </c:if>
                <li>
                    <p>Название фильма: "${ticket.movieSession.movie.name}"</p>
                    <p>Зал: "${ticket.movieSession.hall.name}"</p>
                    <p>Цена: ${ticket.price} грн</p>
                    <p>Дата и время сеанса: ${ticket.movieSession.date.format(FORMAT)}, ${ticket.movieSession.name}</p>
                    <p>Ряд: ${ticket.place.rowNumber}</p>
                    <p>Место: ${ticket.placeNumber}</p>
                </li>
            </c:forEach>
        </ul>
    </div>
</c:if>
<c:if test="${ticketDTOList == null}">
<p>
    У Вас нет Заказов! :)
</p>
</c:if>
</body>
</html>
