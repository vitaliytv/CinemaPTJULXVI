CREATE DATABASE  IF NOT EXISTS `cinema` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `cinema`;
-- MySQL dump 10.13  Distrib 5.7.12, for Win32 (AMD64)
--
-- Host: 127.0.0.1    Database: cinema
-- ------------------------------------------------------
-- Server version	5.7.13-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `hall`
--

DROP TABLE IF EXISTS `hall`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hall` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(75) CHARACTER SET utf8 NOT NULL,
  `description` varchar(300) CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `idHall_UNIQUE` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=cp1251;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hall`
--

LOCK TABLES `hall` WRITE;
/*!40000 ALTER TABLE `hall` DISABLE KEYS */;
INSERT INTO `hall` VALUES (1,'Красный Зал','Красный зал - поддерживает технологии 3D и IMAX'),(2,'Синий Зал','Синий Зал - поддерживает 2D и 3D технологии'),(3,'Зеленый Зал','Зеленый Зал - поддерживает 2D технологию');
/*!40000 ALTER TABLE `hall` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `movie`
--

DROP TABLE IF EXISTS `movie`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `movie` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(150) NOT NULL,
  `year` int(11) NOT NULL,
  `country` varchar(45) NOT NULL,
  `genre` varchar(150) NOT NULL,
  `starring` varchar(200) NOT NULL,
  `director` varchar(60) NOT NULL,
  `length` int(11) NOT NULL,
  `rent_start` date NOT NULL,
  `rent_end` date NOT NULL,
  `description` varchar(1000) NOT NULL,
  `age_limit` varchar(45) NOT NULL,
  `image` varchar(100) NOT NULL,
  `small_image` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `idFilm_UNIQUE` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `movie`
--

LOCK TABLES `movie` WRITE;
/*!40000 ALTER TABLE `movie` DISABLE KEYS */;
INSERT INTO `movie` VALUES (1,'Доктор Стрэндж',2016,'США','фэнтези, приключения, боевик','Бенедикт Камбербэтч, Рейчел МакАдамс, Мадс Миккелсен, Майкл Сталберг, Чиветел Эджиофор, Бенедикт Вонг, Тильда Суинтон','Скотт Дерриксон',115,'2016-10-28','2016-11-23','Лента из комикс-вселенной Marvel «Доктор Стрэндж» повествует об одаренном нейрохирурге из Нью-Йорка докторе Стивене Стрэндже (Бенедикт Камбербэтч). После страшной автокатастрофы, в результате которой обе его руки сильно пострадали, он больше не может практиковать медицину и отправляется за исцелением к Старейшине (Тильда Суинтон). Но та отказывается помочь Стивену, вместо этого знакомя его с магией и параллельными мирами. Так, бывший врач становится супергероем, который надеялся спасти свою карьеру, а теперь должен встать на защиту Вселенной. Ученый не только узнает о существовании необъяснимого и сверхъестественного, но и обретает врагов в лице барона Карла Мордо (Чиветел Эджиофор) и Каэцилуса (Мадс Миккелсен). Доктор Стрэндж становится посредником между метафизической и обычной реальностью, где у него остался единственный родной человек, бывшая коллега Кристин Палмер (Рэйчел МакАдамс).','12+','/resources/image/movies/big_poster_doc_str.jpg','/resources/image/movies/small_poster_doc_str.jpg'),(2,'Тролли',2016,'США','комедия, мультфильм, приключения','Анна Кендрик, Джастин Тимберлейк, Кристин Барански, Кунал Нэйэр, Зоуи Дешанель, Джеймс Корден, Кристофер Минц-Плассе, Джон Клиз','Майк Митчелл, Уолт Дорн',92,'2016-10-27','2016-11-23','Мультфильм «Тролли» открывает зрителю красочный мир забавных существ, задорно танцующих и звонко поющих троллей, среди которых живет никогда не унывающая принцесса Розочка (Анна Кендрик), ди-джей Суки (Гвен Стефани) и самый хмурый из всех троллей Цветан (Джастин Тимберлейк). Две полнейшие противоположности, они должны объединить усилия, чтобы с помощью Бриджет (Зоуи Дешанель) и Крика (Рассел Брэнд) спасти всех обитателей их сказочного мира от злых бергенов, возглавляемых огромным королем по имени Хрящ Старший.','6+','/resources/image/movies/big_poster_trolls.jpg','/resources/image/movies/small_poster_trolls.jpg'),(3,'Джек Ричер 2: Никогда не возвращайся',2016,'США','криминал, приключения, боевик','Том Круз, Роберт Неппер, Даника Ярош, Холт МакКэллани, Коби Смолдерс, Элдис Ходж, Сью-Линн Ансари, Патрик Хьюсингер','Эдвард Цвик',118,'2016-10-20','2016-11-09','Том Круз возвращается в роли непобедимого Джека Ричера в остросюжетном боевике Эдварда Цвика «Джек Ричер 2: Никогда не возвращайся». Странствующий агент Ричер приезжает в родную Вирджинию, где он раньше служил начальником подразделения военной полиции. Там он должен встретиться с нынешней главой отдела, майором Сьюзен Тернер (Коби Смолдерс). Но как только герой прибывает на место, то узнает, что Тернер арестована по обвинению в шпионаже. Ричеру очевидно, что ее подставили, поэтому перед ним встает задача доказать невиновность Сьюзен, выяснив правду о том, кто несет ответственность за убийство солдат, бывших в командовании майора. Но позже дело осложняется еще и тем, что самого Джека Ричера обвиняют в убийстве, якобы совершенном им 16 лет назад.','16+','/resources/image/movies/big_poster_jack_reacher.jpg','/resources/image/movies/small_poster_jack_reacher.jpg'),(4,'Инферно',2016,'Венгрия, США','триллер, детектив','Том Хэнкс, Фелисити Джонс, Омар Си, Ирфан Кхан, Сидсе Бабетт Кнудсен, Бен Фостер, Ана Улару, Ида Дарвиш','Рон Ховард',121,'2016-10-13','2016-11-09','Знаменитый профессор Роберт Лэнгдон (Том Хэнкс) возвращается в третьей части трилогии режиссера Рона Ховарда по бестселлеру Дэна Брауна, «Инферно». Герой просыпается в больничной палате во Флоренции, не понимая, как он туда попал, какой сегодня день и совершенно не помня, что с ним произошло перед этим. Лэнгдон понимает, что его снова преследуют и, возможно, хотят убить. С помощью доктора Сиенны Брукс (Фелисити Джонс) и своих знаний в искусстве и семиотике герой должен вернуть себе свободу, потерянные воспоминания и решить самую сложную загадку, с которой он когда-либо сталкивался – секрет «Божественной комедии» Данте.','16+','/resources/image/movies/big_poster_inferno.jpg','/resources/image/movies/small_poster_inferno.jpg'),(5,'Расплата',2016,'США','триллер, драма, криминал','Бен Аффлек, Анна Кендрик, Дж.К. Симмонс, Джон Бернтал, Джеффри Тэмбор, Синтия Аддай-Робинсон, Джон Литгоу','Гэвин О’Коннор',128,'2016-10-27','2016-11-16','Кристиан Вульф (Бен Аффлек) был странным, но одаренным ребенком с незаурядным умом и яркими аналитическими способностями. Повзрослев, замкнутый бухгалтер начал вести опасную двойную жизнь. В одной своей ипостаси Кристиан – сотрудник маленького офиса, безупречно разбирающийся во всех тонкостях документации. В другой – математический гений, подрабатывающий финансовым консультантом у крупнейших криминальных организаций и весьма опасных личностей. Однажды им заинтересовывается Рей Кинг (Дж.К. Симмонс), начальник отдела по борьбе с финансовой преступностью, и Вульфу приходится подыскать себе более добропорядочного клиента. Им оказывается компания, производящая робототехнику. Аудиторская проверка фирмы выявляет недостачу в несколько миллионов долларов, но когда Кристиан пытается разобраться в этом вместе с сотрудницей Даной (Анна Кендрик), начинается череда загадочных убийств.','18+','/resources/image/movies/big_poster_accountant.jpg','/resources/image/movies/small_poster_accountant.jpg');
/*!40000 ALTER TABLE `movie` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `moviesession`
--

DROP TABLE IF EXISTS `moviesession`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `moviesession` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) CHARACTER SET utf8 NOT NULL,
  `date` datetime NOT NULL,
  `movie_id` int(11) NOT NULL,
  `hall_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `idFilmSession_UNIQUE` (`id`),
  KEY `film_id_idx` (`movie_id`),
  KEY `hall_id_idx` (`hall_id`),
  CONSTRAINT `film_id` FOREIGN KEY (`movie_id`) REFERENCES `movie` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `hall_idd` FOREIGN KEY (`hall_id`) REFERENCES `hall` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=70 DEFAULT CHARSET=cp1251;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `moviesession`
--

LOCK TABLES `moviesession` WRITE;
/*!40000 ALTER TABLE `moviesession` DISABLE KEYS */;
INSERT INTO `moviesession` VALUES (16,'13:15','2016-11-06 00:00:00',1,2),(17,'19:40','2016-11-06 00:00:00',1,2),(18,'23:15','2016-11-06 00:00:00',1,2),(19,'13:15','2016-11-07 00:00:00',1,2),(20,'19:40','2016-11-07 00:00:00',1,2),(21,'23:15','2016-11-07 00:00:00',1,2),(22,'13:15','2016-11-08 00:00:00',1,2),(23,'19:40','2016-11-08 00:00:00',1,2),(24,'23:15','2016-11-08 00:00:00',1,2),(25,'12:30','2016-11-06 00:00:00',1,3),(26,'17:30','2016-11-06 00:00:00',1,3),(27,'22:30','2016-11-06 00:00:00',1,3),(28,'12:30','2016-11-07 00:00:00',1,3),(29,'17:30','2016-11-07 00:00:00',1,3),(30,'22:30','2016-11-07 00:00:00',1,3),(31,'12:30','2016-11-08 00:00:00',1,3),(32,'17:30','2016-11-08 00:00:00',1,3),(33,'22:30','2016-11-08 00:00:00',1,3),(34,'11:00','2016-11-06 00:00:00',2,3),(35,'14:00','2016-11-06 00:00:00',2,3),(36,'11:00','2016-11-07 00:00:00',2,3),(37,'14:00','2016-11-07 00:00:00',2,3),(38,'11:00','2016-11-08 00:00:00',2,3),(39,'14:00','2016-11-08 00:00:00',2,3),(40,'12:15','2016-11-06 00:00:00',2,2),(41,'17:30','2016-11-06 00:00:00',2,2),(42,'12:15','2016-11-07 00:00:00',2,2),(43,'17:30','2016-11-07 00:00:00',2,2),(44,'12:15','2016-11-08 00:00:00',2,2),(45,'17:30','2016-11-08 00:00:00',2,2),(46,'16:30','2016-11-06 00:00:00',2,1),(47,'16:30','2016-11-07 00:00:00',2,1),(48,'16:30','2016-11-08 00:00:00',2,1),(49,'10:00','2016-11-06 00:00:00',3,3),(50,'10:00','2016-11-07 00:00:00',3,3),(51,'10:00','2016-11-08 00:00:00',3,3),(52,'23:30','2016-11-06 00:00:00',3,1),(53,'23:30','2016-11-07 00:00:00',3,1),(54,'23:30','2016-11-08 00:00:00',3,1),(55,'17:30','2016-11-06 00:00:00',4,3),(56,'17:30','2016-11-07 00:00:00',4,3),(57,'17:30','2016-11-08 00:00:00',4,3),(58,'15:45','2016-11-06 00:00:00',5,3),(59,'20:15','2016-11-06 00:00:00',5,3),(60,'15:45','2016-11-07 00:00:00',5,3),(61,'20:15','2016-11-07 00:00:00',5,3),(62,'15:45','2016-11-08 00:00:00',5,3),(63,'20:15','2016-11-08 00:00:00',5,3),(64,'18:30','2016-11-06 00:00:00',1,1),(65,'21:00','2016-11-06 00:00:00',1,1),(66,'18:30','2016-11-07 00:00:00',1,1),(67,'21:00','2016-11-07 00:00:00',1,1),(68,'18:30','2016-11-08 00:00:00',1,1),(69,'21:00','2016-11-08 00:00:00',1,1);
/*!40000 ALTER TABLE `moviesession` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `place`
--

DROP TABLE IF EXISTS `place`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `place` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `row_number` int(11) NOT NULL,
  `place_number` int(11) NOT NULL,
  `hall_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `idStructureOfRow_UNIQUE` (`id`),
  KEY `hall_id_idx` (`hall_id`),
  CONSTRAINT `hall_id` FOREIGN KEY (`hall_id`) REFERENCES `hall` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=cp1251;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `place`
--

LOCK TABLES `place` WRITE;
/*!40000 ALTER TABLE `place` DISABLE KEYS */;
INSERT INTO `place` VALUES (1,1,12,1),(2,2,12,1),(3,3,12,1),(4,4,12,1),(5,5,12,1),(6,6,12,1),(7,7,12,1),(8,8,12,1),(9,9,12,1),(10,1,15,2),(11,2,15,2),(12,3,15,2),(13,4,15,2),(14,5,15,2),(15,6,15,2),(16,7,15,2),(17,8,15,2),(18,9,15,2),(19,10,15,2),(20,11,15,2),(21,12,15,2),(22,1,17,3),(23,2,17,3),(24,3,17,3),(25,4,17,3),(26,5,17,3),(27,6,17,3),(28,7,17,3),(29,8,17,3),(30,9,17,3),(31,10,17,3),(32,11,17,3),(33,12,17,3),(34,13,17,3),(35,14,17,3);
/*!40000 ALTER TABLE `place` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role`
--

DROP TABLE IF EXISTS `role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(75) CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=cp1251;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role`
--

LOCK TABLES `role` WRITE;
/*!40000 ALTER TABLE `role` DISABLE KEYS */;
INSERT INTO `role` VALUES (1,'admin'),(2,'user');
/*!40000 ALTER TABLE `role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ticket`
--

DROP TABLE IF EXISTS `ticket`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ticket` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date` date NOT NULL,
  `ticket_condition` int(11) NOT NULL,
  `price` decimal(10,0) NOT NULL,
  `moviesession_id` int(11) NOT NULL,
  `place_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `place_number` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `idTicket_UNIQUE` (`id`),
  KEY `place_id_idx` (`place_id`),
  KEY `user_id_idx` (`user_id`),
  KEY `filmsession_id_idx` (`moviesession_id`),
  CONSTRAINT `moviesession_id` FOREIGN KEY (`moviesession_id`) REFERENCES `moviesession` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `place_id` FOREIGN KEY (`place_id`) REFERENCES `place` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=cp1251;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ticket`
--

LOCK TABLES `ticket` WRITE;
/*!40000 ALTER TABLE `ticket` DISABLE KEYS */;
INSERT INTO `ticket` VALUES (1,'2016-11-04',1,50,21,17,2,7),(2,'2016-11-04',1,50,21,17,2,8),(3,'2016-11-04',1,50,45,14,2,6),(4,'2016-11-04',1,50,45,14,2,7),(5,'2016-11-04',1,50,45,14,2,8);
/*!40000 ALTER TABLE `ticket` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(75) NOT NULL,
  `surname` varchar(75) NOT NULL,
  `email` varchar(100) NOT NULL,
  `login` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `phone` int(20) NOT NULL,
  `birthday` date NOT NULL,
  `sex` varchar(45) NOT NULL,
  `role_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `idUser_UNIQUE` (`id`),
  KEY `role_id_idx` (`role_id`),
  CONSTRAINT `role_id` FOREIGN KEY (`role_id`) REFERENCES `role` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,'Vitaliy','Tarasenko','vitalik@gmail.com','admin','admin',95823,'1988-11-03','male',1),(2,'Andy','Vasylenko','andy@ya.ru','andy','andy',9345,'1990-09-11','male',2),(6,'Петр','Петров','p@pav.com','pav','5555',958213,'1999-11-09','male',2);
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping routines for database 'cinema'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-11-04 17:07:51
